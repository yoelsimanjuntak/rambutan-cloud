<?php
class Master extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }
    if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
      redirect('site/user/dashboard');
    }
  }

  public function region() {
    $data['title'] = "Daftar Kelurahan";
    $this->template->load('adminlte', 'master/region', $data);
  }

  public function region_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $questStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;
    //$dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    //$dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_REGIONNAMA,null,COL_CREATEDON,COL_CREATEDBY);
    $cols = array(COL_REGIONNAMA,COL_REGIONKETERANGAN,COL_CREATEDON,COL_CREATEDBY);

    $queryAll = $this->db
    ->where(COL_ISDELETED, 0)
    ->get(TBL_MREGION);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_REGIONNAMA, 'asc')
    ->get_compiled_select(TBL_MREGION, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $createdby = $r[COL_CREATEDBY];
      $createdon = date('Y-m-d H:i', strtotime($r[COL_CREATEDON]));
      $updatedby = $r[COL_UPDATEDBY];
      $updatedon = date('Y-m-d H:i', strtotime($r[COL_UPDATEDON]));

      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/region-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times"></i>&nbsp;HAPUS</a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/master/region-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-info btn-edit" data-name="'.$r[COL_REGIONNAMA].'" data-keterangan="'.$r[COL_REGIONKETERANGAN].'" data-createdby="'.$createdby.'" data-createdon="'.$createdon.'" data-updatedby="'.$updatedby.'" data-updatedon="'.$updatedon.'"><i class="fas fa-edit"></i> UBAH</a>';

      $data[] = array(
        $htmlBtn,
        $r[COL_REGIONNAMA],
        $r[COL_REGIONKETERANGAN],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON])),
        $r[COL_CREATEDBY],
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function region_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_REGIONNAMA => $this->input->post(COL_REGIONNAMA),
        COL_REGIONKETERANGAN => $this->input->post(COL_REGIONKETERANGAN),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MREGION, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Entri data <strong>BERHASIL</strong>!');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid!');
      return;
    }
  }

  public function region_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_REGIONNAMA => $this->input->post(COL_REGIONNAMA),
        COL_REGIONKETERANGAN => $this->input->post(COL_REGIONKETERANGAN),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MREGION, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Entri data <strong>BERHASIL</strong>!');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid!');
      return;
    }
  }

  public function region_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MREGION, array(COL_ISDELETED=>1));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Penghapusan data <strong>BERHASIL</strong>!');
  }

  public function unit() {
    $data['title'] = "Daftar Unit Kerja";
    $this->template->load('adminlte', 'master/unit', $data);
  }

  public function unit_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $questStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;
    //$dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    //$dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_UNITNAMA,COL_UNITPIMPINAN,null,COL_CREATEDON,COL_CREATEDBY);
    $cols = array(COL_UNITNAMA,COL_UNITPIMPINAN,COL_UNITALAMAT,COL_CREATEDON,COL_CREATEDBY);

    $queryAll = $this->db
    ->where(COL_ISDELETED, 0)
    ->get(TBL_MUNIT);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->where(COL_ISDELETED, 0)
    ->order_by(COL_UNITNAMA, 'asc')
    ->get_compiled_select(TBL_MUNIT, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $createdby = $r[COL_CREATEDBY];
      $createdon = date('Y-m-d H:i', strtotime($r[COL_CREATEDON]));
      $updatedby = $r[COL_UPDATEDBY];
      $updatedon = date('Y-m-d H:i', strtotime($r[COL_UPDATEDON]));

      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/unit-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times"></i>&nbsp;HAPUS</a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/master/unit-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-info btn-edit" data-name="'.$r[COL_UNITNAMA].'" data-pimpinan="'.$r[COL_UNITPIMPINAN].'" data-keterangan="'.$r[COL_UNITALAMAT].'" data-createdby="'.$createdby.'" data-createdon="'.$createdon.'" data-updatedby="'.$updatedby.'" data-updatedon="'.$updatedon.'"><i class="fas fa-edit"></i> UBAH</a>';

      $data[] = array(
        $htmlBtn,
        $r[COL_UNITNAMA],
        $r[COL_UNITPIMPINAN],
        $r[COL_UNITALAMAT],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON])),
        $r[COL_CREATEDBY],
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function unit_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_UNITNAMA => $this->input->post(COL_UNITNAMA),
        COL_UNITPIMPINAN => $this->input->post(COL_UNITPIMPINAN),
        COL_UNITALAMAT => $this->input->post(COL_UNITALAMAT),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MUNIT, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Entri data <strong>BERHASIL</strong>!');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid!');
      return;
    }
  }

  public function unit_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_UNITNAMA => $this->input->post(COL_UNITNAMA),
        COL_UNITPIMPINAN => $this->input->post(COL_UNITPIMPINAN),
        COL_UNITALAMAT => $this->input->post(COL_UNITALAMAT),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MUNIT, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Entri data <strong>BERHASIL</strong>!');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid!');
      return;
    }
  }

  public function unit_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MUNIT, array(COL_ISDELETED=>1));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Penghapusan data <strong>BERHASIL</strong>!');
  }

  public function services() {
    $data['title'] = "Daftar Layanan";
    $this->template->load('adminlte', 'master/service', $data);
  }

  public function services_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $questStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;
    //$dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    //$dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_LAYANANNAMA,null,null,COL_CREATEDON,COL_CREATEDBY);
    $cols = array(COL_LAYANANNAMA,COL_LAYANANDURASI,COL_LAYANANKETERANGAN,COL_CREATEDON,COL_CREATEDBY);

    $queryAll = $this->db
    ->where(COL_ISDELETED, 0)
    ->get(TBL_MLAYANAN);

    $i = 0;
    foreach($cols as $item){
      if($item==COL_CREATEDON) $item = TBL_MLAYANAN.'.'.COL_CREATEDON;
      else if($item==COL_CREATEDBY) $item = TBL_MLAYANAN.'.'.COL_CREATEDBY;
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      if($orderables[$_POST['order']['0']['column']]==COL_CREATEDON) $this->db->order_by(TBL_MLAYANAN.'.'.COL_CREATEDON, $_POST['order']['0']['dir']);
      else if($orderables[$_POST['order']['0']['column']]==COL_CREATEDBY) $this->db->order_by(TBL_MLAYANAN.'.'.COL_CREATEDBY, $_POST['order']['0']['dir']);
      else $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->where(TBL_MLAYANAN.'.'.COL_ISDELETED, 0)
    ->order_by(COL_LAYANANNAMA, 'asc')
    ->get_compiled_select(TBL_MLAYANAN, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $createdby = $r[COL_CREATEDBY];
      $createdon = date('Y-m-d H:i', strtotime($r[COL_CREATEDON]));
      $updatedby = $r[COL_UPDATEDBY];
      $updatedon = date('Y-m-d H:i', strtotime($r[COL_UPDATEDON]));

      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/master/services-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times"></i>&nbsp;HAPUS</a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/master/services-edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-info btn-edit" data-name="'.$r[COL_LAYANANNAMA].'" data-durasi="'.$r[COL_LAYANANDURASI].'" data-keterangan="'.$r[COL_LAYANANKETERANGAN].'" data-createdby="'.$createdby.'" data-createdon="'.$createdon.'" data-updatedby="'.$updatedby.'" data-updatedon="'.$updatedon.'"><i class="fas fa-edit"></i> UBAH</a>';

      $data[] = array(
        $htmlBtn,
        $r[COL_LAYANANNAMA],
        number_format($r[COL_LAYANANDURASI]).' menit',
        $r[COL_LAYANANKETERANGAN],
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON])),
        $r[COL_CREATEDBY],
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function services_add() {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_LAYANANNAMA => $this->input->post(COL_LAYANANNAMA),
        COL_LAYANANDURASI => $this->input->post(COL_LAYANANDURASI),
        COL_LAYANANKETERANGAN => $this->input->post(COL_LAYANANKETERANGAN),
        COL_CREATEDBY => $ruser[COL_USERNAME],
        COL_CREATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_MLAYANAN, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Entri data <strong>BERHASIL</strong>!');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid!');
      return;
    }
  }

  public function services_edit($id) {
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_LAYANANNAMA => $this->input->post(COL_LAYANANNAMA),
        COL_LAYANANDURASI => $this->input->post(COL_LAYANANDURASI),
        COL_LAYANANKETERANGAN => $this->input->post(COL_LAYANANKETERANGAN),
        COL_UPDATEDBY => $ruser[COL_USERNAME],
        COL_UPDATEDON => date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MLAYANAN, $data);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception('Error: '.$err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('Entri data <strong>BERHASIL</strong>!');
        return;
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        return;
      }
    } else {
      ShowJsonError('Parameter tidak valid!');
      return;
    }
  }

  public function services_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_MLAYANAN, array(COL_ISDELETED=>1));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('Penghapusan data <strong>BERHASIL</strong>!');
  }
}
