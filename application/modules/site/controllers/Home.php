<?php
class Home extends MY_Controller {

  public function sendmail() {
    /*$this->load->library('Mailer');
    $mail = $this->mailer->load();
    //$mail = new PHPMailer();
    $mail->IsSMTP();
    $mail->Mailer = "smtp";
    $mail->SMTPDebug  = 3;
    $mail->SMTPAuth   = TRUE;
    $mail->SMTPSecure = "ssl";
    $mail->Port       = 587;
    $mail->Host       = "tls://smtp.gmail.com";
    $mail->Username   = "sasuke.tebingtinggi@gmail.com";
    $mail->Password   = "ikitmdrovkunwflm";
    $mail->IsHTML(true);
    $mail->AddAddress("yoelrolas@gmail.com");
    $mail->SetFrom("sasuke.tebingtinggi@gmail.com", "SASUKE Kecamatan Rambutan");
    $mail->Subject = "Test is Test Email sent via Gmail SMTP Server using PHP Mailer";
    $content = "<b>This is a Test Email sent via Gmail SMTP Server using PHP mailer class.</b>";
    $mail->MsgHTML($content);
    if(!$mail->Send()) {
      echo "Error while sending Email.";
      var_dump($mail);
    } else {
      echo "Email sent successfully";
    }*/
    $param = array(
      'preheader'=>'',
      'txtmain'=>'',
      'txtURL'=>'',
      'txtLink'=>'',
      'txtsub'=>''
    );
    $html = $this->load->view('site/ticket/mail', $param, TRUE);
    echo SendMail('yoelrolas@gmail.com', 'Testing', $html);
  }

  public function index() {
    //echo SendMail('yoelrolas@gmail.com', 'Subjek', 'Rambutan Cloud');
    $data['title'] = 'Beranda';
    $this->load->view('site/home/index', $data);
  }

  public function page($slug) {
    $this->load->model('mpost');
    $data['title'] = 'Page';

    $this->db->select('_posts.*, _postcategories.PostCategoryName, users.Fullname');
    $this->db->join(TBL__POSTCATEGORIES,TBL__POSTCATEGORIES.'.'.COL_POSTCATEGORYID." = ".TBL__POSTS.".".COL_POSTCATEGORYID,"inner");
    $this->db->join(TBL_USERS,TBL_USERS.'.'.COL_USERNAME." = ".TBL__POSTS.".".COL_CREATEDBY,"inner");
    $this->db->where("(".TBL__POSTS.".".COL_POSTSLUG." = '".$slug."' OR ".TBL__POSTS.".".COL_POSTID." = '".$slug."')");
    $rpost = $this->db->get(TBL__POSTS)->row_array();
    if(!$rpost) {
        show_404();
        return false;
    }

    $this->db->where(COL_POSTID, $rpost[COL_POSTID]);
    $this->db->set(COL_TOTALVIEW, COL_TOTALVIEW."+1", FALSE);
    $this->db->update(TBL__POSTS);

    $data['title'] = $rpost[COL_POSTTITLE];
    $data['data'] = $rpost;
    $data['berita'] = $this->mpost->search(5,"",1);
    $this->template->load('shards' , 'home/page', $data);
  }

  public function _404() {
    $data['title'] = 'Error';
    if(IsLogin()) {
      $this->template->load('adminlte' , 'home/_error', $data);
    } else {
      $this->template->load('adminlte' , 'home/_error', $data);
    }
  }

  public function testimoni() {
    $data['title'] = 'Testimoni';
    $this->load->view('site/home/testimoni', $data);
  }

  public function testimoni_add() {
    if(!empty($_POST)) {
      $this->db->trans_begin();
      try {
        $rec = array(
          COL_CONTENTTYPE=>'Testimonial',
          COL_CONTENTTITLE=>$this->input->post('TestimoniTitle'),
          COL_CONTENTDESC1=>$this->input->post('TestimoniName'),
          COL_CONTENTDESC2=>$this->input->post('TestimoniText'),
        );

        $res = $this->db->insert(TBL_WEBCONTENT, $rec);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $this->db->trans_commit();
        ShowJsonSuccess('SELESAI');
        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    }
  }
}
 ?>
