<?php
class Ticket extends MY_Controller {
  function __construct() {
    parent::__construct();
  }

  public function index($ticketno='') {
    $data['ticketno'] = $ticketno;
    $data['title'] = 'Tiket Permohonan';
    $this->template->load('shards', 'ticket/index', $data);
  }

  public function add() {
    if(!empty($_POST)) {
      $config['upload_path'] = MY_UPLOADPATH;
      $config['allowed_types'] = "jpg|jpeg|png|pdf";
      $config['max_size']	= 102400;
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);
      if(empty($_FILES)) {
        ShowJsonError('File yang diunggah tidak valid!');
        exit();
      }

      $fileKTP = '';
      $fileKK = '';
      $res = $this->upload->do_upload('file-ktp');
      if(!$res) {
        $err = $this->upload->display_errors('', '');
        ShowJsonError($err);
        exit();
      }
      $upl = $this->upload->data();
      $fileKTP = $upl['file_name'];

      $res = $this->upload->do_upload('file-kk');
      if(!$res) {
        $err = $this->upload->display_errors('', '');
        ShowJsonError($err);
        exit();
      }
      $upl = $this->upload->data();
      $fileKK = $upl['file_name'];

      $numrows = $this->db->count_all_results(TBL_TRTIKET);
      $rec = array(
        COL_IDLAYANAN=>$this->input->post(COL_IDLAYANAN),
        COL_IDREGION=>$this->input->post(COL_IDREGION),
        COL_TIKETNO=>date('Ymd').str_pad($numrows+1,4,"0",STR_PAD_LEFT),
        COL_TIKETNAMA=>$this->input->post(COL_TIKETNAMA),
        COL_TIKETNIK=>$this->input->post(COL_TIKETNIK),
        COL_TIKETALAMAT=>$this->input->post(COL_TIKETALAMAT),
        COL_TIKETEMAIL=>$this->input->post(COL_TIKETEMAIL),
        COL_TIKETHP=>$this->input->post(COL_TIKETHP),
        COL_TIKETFILE1=>$fileKTP,
        COL_TIKETFILE2=>$fileKK,
        COL_TIKETMAKSUD=>$this->input->post(COL_TIKETMAKSUD),
        COL_TIKETTUJUAN=>$this->input->post(COL_TIKETTUJUAN),
        COL_CREATEDON=>date('Y-m-d H:i:s')
      );

      $this->db->trans_begin();
      try {
        $res = $this->db->insert(TBL_TRTIKET, $rec);
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }

        $idTiket = $this->db->insert_id();
        $res = $this->db->insert(TBL_TRTIKETSTATUS, array(COL_IDTIKET=>$idTiket, COL_IDSTATUS=>STATUS_BARU, COL_CREATEDON=>date('Y-m-d H:i:s')));
        if(!$res) {
          $err = $this->db->error();
          throw new Exception($err['message']);
        }
        $this->db->trans_commit();
        ShowJsonSuccess('Tiket anda sudah kami terima dengan nomor '.$rec[COL_TIKETNO].'. Anda akan mendapatkan notifikasi via email saat tiket anda diproses.', array('ajax'=>'https://chayrasmart.com/site/home/sasuke-sendmail/'.urlencode($rec[COL_TIKETEMAIL]).'/'.$rec[COL_TIKETNO]));

        /*$txtmail = @"
        <p>Terimakasih telah menyampaikan permohonan Surat Keterangan melalui aplikasi <strong>".$this->setting_web_name."</strong> (".$this->setting_web_desc.").</p>
        <p>Tiket anda sudah kami terima dengan nomor <strong>".$rec[COL_TIKETNO]."</strong>. Anda akan mendapatkan notifikasi via email saat tiket anda diproses / selesai.</p>
        <p>Anda dapat memantau status tiket anda melalui link dibawah ini.</p>
        ";
        $mailParam = array(
          'preheader'=>'Terimakasih telah menyampaikan permohonan Surat Keterangan melalui aplikasi '.$this->setting_web_name.'.',
          'txtmain'=>$txtmail,
          'txtURL'=>site_url('site/ticket/index/'.$rec[COL_TIKETNO]),
          'txtlink'=>'STATUS TIKET',
          'txtsub'=>'<p>Untuk informasi lebih lanjut, anda dapat menghubungi nomor <strong>'.$this->setting_org_phone.'</strong> atau mengirim email ke '.$this->setting_org_mail.'.</p>'
        );
        $mailHtml = $this->load->view('site/ticket/mail', $mailParam, TRUE);
        SendMail($rec[COL_TIKETEMAIL], 'Pendaftaran Tiket No. '.$rec[COL_TIKETNO].' Berhasil', $mailHtml);*/

        exit();
      } catch(Exception $ex) {
        $this->db->trans_rollback();
        ShowJsonError($ex->getMessage());
        exit();
      }
    } else {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }
  }

  public function info() {
    $noTiket = $this->input->post('TiketNo');
    $res = $this->db
    ->select('trtiket.*, mregion.RegionNama, mlayanan.LayananNama')
    ->join(TBL_MREGION,TBL_MREGION.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDREGION,"left")
    ->join(TBL_MLAYANAN,TBL_MLAYANAN.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDLAYANAN,"left")
    ->where(COL_TIKETNO, $noTiket)
    ->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, 'desc')
    ->get(TBL_TRTIKET)
    ->row_array();

    if(empty($res)) {
      echo @
      '<div class="row mt-4">
        <div class="col-sm-6">
          <p class="font-italic">Maaf, nomor tiket <strong>'.$noTiket.'</strong> tidak terdaftar di sistem. Silakan periksa kembali nomor tiket yang tertera pada notifikasi email anda.</p>
        </div>
      </div>';
      exit();
    }

    $this->load->view('site/ticket/info', array('res'=>$res));
  }

  public function rate($id, $rate) {
    $rdata = $this->db
    ->select('trtiket.*, mregion.RegionNama, mlayanan.LayananNama')
    ->join(TBL_MREGION,TBL_MREGION.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDREGION,"left")
    ->join(TBL_MLAYANAN,TBL_MLAYANAN.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDLAYANAN,"left")
    ->where(TBL_TRTIKET.'.'.COL_UNIQ, $id)
    ->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, 'desc')
    ->get(TBL_TRTIKET)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRTIKET, array(COL_TIKETKEPUASAN=>strtoupper(str_replace("-"," ",$rate))));
    if(!$res) {
      $err = $this->db->error();
      //throw new Exception($err['message']);
    }

    redirect(site_url('site/ticket/index/'.$rdata[COL_TIKETNO]));
  }
}
?>
