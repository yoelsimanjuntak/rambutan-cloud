<?php
class User extends MY_Controller {
  function __construct() {
      parent::__construct();
      //$this->load->library('encrypt');
      if(IsLogin() && GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
        //redirect('site/home');
      }
  }

  function index() {
    $data['title'] = "Daftar Pengguna";
    $this->template->load('adminlte', 'user/index', $data);
  }

  public function index_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $userStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_USERNAME,COL_FULLNAME,COL_PHONE,null,COL_CREATEDON);
    $cols = array(COL_USERNAME, COL_FULLNAME, COL_PHONE);

    $queryAll = $this->db
    ->where(COL_ROLEID, ROLEUSER)
    ->get(TBL_USERS);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    /*if(!empty($dateFrom)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where(TBL_TSTOCKDISTRIBUTION.'.'.COL_DATEDISTRIBUTION.' <= ', $dateTo);
    }*/

    if(!empty($userStatus)) {
      if($userStatus==1) {
        $this->db->where(COL_ISSUSPEND, 0);
      } else {
        $this->db->where(COL_ISSUSPEND, 1);
      }
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->where(COL_ROLEID, ROLEUSER)
    ->get_compiled_select(TBL_USERS, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/user/edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit"><i class="fas fa-edit"></i>&nbsp;UBAH</a>&nbsp;';
      if($r[COL_ISSUSPEND]==0) {
        $htmlBtn .= '<a href="'.site_url('site/user/activation/'.$r[COL_UNIQ].'/0').'" class="btn btn-xs btn-outline-danger btn-action"><i class="fas fa-times-circle"></i>&nbsp;SUSPEND</a>';
      } else {
        $htmlBtn .= '<a href="'.site_url('site/user/activation/'.$r[COL_UNIQ].'/1').'" class="btn btn-xs btn-outline-success btn-action"><i class="fas fa-check-circle"></i>&nbsp;AKTIFKAN</a>';
      }

      $data[] = array(
        $htmlBtn,
        $r[COL_USERNAME],
        $r[COL_FULLNAME],
        $r[COL_PHONE],
        ($r[COL_ISSUSPEND]==0?'<i class="far fa-check-circle text-success"></i>':'<i class="far fa-times-circle text-danger"></i>'),
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON]))
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  function Login(){
    if(IsLogin()) {
      redirect('site/user/dashboard');
    }
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'Username',
          'label' => 'Username',
          'rules' => 'required'
        ),
        array(
          'field' => 'Password',
          'label' => 'Password',
          'rules' => 'required'
        )
      ));
      if($this->form_validation->run()) {
        $username = $this->input->post(COL_USERNAME);
        $password = $this->input->post(COL_PASSWORD);

        $ruser = $this->db
        ->where(COL_USERNAME,$username)
        ->where(COL_PASSWORD,md5($password))
        ->get(TBL_USERS)
        ->row_array();

        if(empty($ruser)) {
          ShowJsonError('Maaf, username / password yang anda masukkan tidak valid. Silakan coba kembali.');
          exit();
        }
        if($ruser[COL_ROLEID]!=ROLEADMIN) {
          if($ruser[COL_ISEMAILVERIFIED]!=1) {
            //ShowJsonError('Maaf, email anda belum terverifikasi. Silakan lakukan verifikasi melalui link yang dikirim ke email anda terlebih dahulu.');
            //exit();
          }

          if($ruser[COL_ISSUSPEND]==1) {
            ShowJsonError('Maaf, akun anda untuk sementara di SUSPEND. Silakan hubungi administrator.');
            exit();
          }
        }

        /*
        $this->db->where(COL_USERNAME, $username);
        $this->db->update(TBL__USERS, array(COL_LASTLOGIN=>date('Y-m-d H:i:s'), COL_LASTLOGINIP=>$this->input->ip_address()));
        */

        SetLoginSession($ruser);
        ShowJsonSuccess('Selamat datang kembali, <strong>'.$ruser[COL_FULLNAME].'</strong>!', array('redirect'=>site_url('site/user/dashboard')));
        exit();

      } else {
        ShowJsonError('Maaf, username / password yang anda masukkan tidak valid. Silakan coba kembali.');
        exit();
      }
    } else {
      $this->load->view('site/user/login');
    }
  }
  function Logout(){
      UnsetLoginSession();
      redirect(site_url());
  }
  function Dashboard() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $data['title'] = 'Dashboard';
    $this->template->load('adminlte', 'site/user/dashboard', $data);
  }

  public function add() {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError('ANDA TIDAK MEMILIKI HAK AKSES.');
        exit();
      }
      $rexist = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_EMAIL))
      ->get(TBL_USERS)
      ->row_array();
      if(!empty($rexist)) {
        ShowJsonError('Maaf, alamat email <strong>'.$rexist[COL_EMAIL].'</strong> telah terdaftar.');
        exit();
      }

      $data = array(
        COL_ROLEID=>ROLEUSER,
        COL_FULLNAME=>$this->input->post(COL_FULLNAME),
        COL_USERNAME=>$this->input->post(COL_EMAIL),
        COL_EMAIL=>$this->input->post(COL_EMAIL),
        COL_PHONE=>$this->input->post(COL_PHONE),
        COL_PASSWORD=>md5($this->input->post(COL_PASSWORD)),
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $res = $this->db->insert(TBL_USERS, $data);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('BERHASIL');
      exit();
    } else {
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        show_error('ANDA TIDAK MEMILIKI HAK AKSES.');
        exit();
      }
      $this->load->view('site/user/form');
    }
  }

  public function edit($id) {
    if(!IsLogin()) {
      redirect('site/user/login');
    }

    $ruser = GetLoggedUser();

    $data['data'] = $rdata = $this->db
    ->where(COL_UNIQ, $id)
    ->get(TBL_USERS)
    ->row_array();

    if(empty($data)) {
      ShowJsonError('PARAMETER TIDAK VALID');
      exit();
    }

    if(!empty($_POST)) {
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        ShowJsonError('ANDA TIDAK MEMILIKI HAK AKSES.');
        exit();
      }

      $data = array(
        COL_FULLNAME=>$this->input->post(COL_FULLNAME),
        COL_USERNAME=>$this->input->post(COL_EMAIL),
        COL_EMAIL=>$this->input->post(COL_EMAIL),
        COL_PHONE=>$this->input->post(COL_PHONE),
        COL_UPDATEDON=>date('Y-m-d H:i:s'),
        COL_UPDATEDBY=>$ruser[COL_USERNAME]
      );
      if(!empty($this->input->post(COL_PASSWORD))) {
        $data[COL_PASSWORD] = md5($this->input->post(COL_PASSWORD));
      }

      $res = $this->db->where(COL_UNIQ, $id)->update(TBL_USERS, $data);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('BERHASIL');
      exit();
    } else {
      if($ruser[COL_ROLEID] != ROLEADMIN) {
        show_error('ANDA TIDAK MEMILIKI HAK AKSES.');
        exit();
      }

      $this->load->view('site/user/form', $data);
    }
  }

  public function activation($id, $stat=0) {
    $res = $this->db->where(COL_UNIQ, $id)->update(TBL_USERS, array(COL_ISSUSPEND=>$stat==0?1:0));
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL');
    exit();
  }

  public function changepassword() {
    $data['data'] = array();
    $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'NewPassword',
          'label' => 'NewPassword',
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[NewPassword]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password Baru.')
        )
      ));

      if($ruser[COL_PASSWORD] != md5($this->input->post('OldPassword'))) {
        ShowJsonError('Password Lama tidak valid.');
        return false;
      }

      if($this->form_validation->run()) {
        $res = $this->db
        ->where(COL_USERNAME, $ruser[COL_USERNAME])
        ->update(TBL_USERS, array(COL_PASSWORD=>md5($this->input->post('NewPassword'))));
        if(!$res) {
          $err = $this->db->error();
          ShowJsonError($err['message']);
          return false;
        }

        ShowJsonSuccess('Password berhasil diperbarui.', array('redirect'=>site_url('site/user/logout')));
        return false;
      } else {
        ShowJsonError(validation_errors());
        return false;
      }
    } else {
      $this->load->view('site/user/changepassword', $data);
    }
  }

  public function register() {
    if(!empty($_POST)) {
      $this->form_validation->set_rules(array(
        array(
          'field' => 'Password',
          'label' => 'Password',
          'rules' => 'required|min_length[5]',
          'errors' => array('min_length' => 'Password minimal terdiri dari 5 karakter.')
        ),
        array(
          'field' => 'ConfirmPassword',
          'label' => 'Repeat ConfirmPassword',
          'rules' => 'required|matches[Password]',
          'errors' => array('matches' => 'Kolom Konfirmasi Password wajib sama dengan Password Baru.')
        )
      ));

      $rexist = $this->db
      ->where(COL_USERNAME, $this->input->post(COL_EMAIL))
      ->get(TBL_USERS)
      ->row_array();
      if(!empty($rexist)) {
        ShowJsonError('Maaf, alamat email <strong>'.$rexist[COL_EMAIL].'</strong> telah terdaftar. Silakan daftar menggunakan email yang belum pernah terdaftar sebelumnya.');
        exit();
      }

      if(!$this->form_validation->run()) {
        $err = htmlspecialchars_decode(strip_tags(validation_errors()));
        ShowJsonError($err);
        return false;
      }

      $data = array(
        COL_ROLEID=>ROLEUSER,
        COL_FULLNAME=>$this->input->post(COL_FULLNAME),
        COL_USERNAME=>$this->input->post(COL_EMAIL),
        COL_EMAIL=>$this->input->post(COL_EMAIL),
        COL_PHONE=>$this->input->post(COL_PHONE),
        COL_PASSWORD=>md5($this->input->post(COL_PASSWORD)),
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$this->input->post(COL_EMAIL)
      );

      $res = $this->db->insert(TBL_USERS, $data);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      ShowJsonSuccess('Pendaftaran berhasil. Silakan login menggunakan akun yang sudah terdaftar.', array('redirect'=>site_url('site/user/login')));
      exit();
    } else {
      $this->load->view('site/user/register');
    }
  }

  public function profile() {
    $data['data'] = $ruser = GetLoggedUser();
    if(!empty($_POST)) {
      $data = array(
        COL_FULLNAME=>$this->input->post(COL_FULLNAME),
        COL_GENDER=>$this->input->post(COL_GENDER),
        COL_PHONE=>$this->input->post(COL_PHONE),
        COL_DATEBIRTH=>date('Y-m-d', strtotime($this->input->post(COL_DATEBIRTH)))
      );
      $res = $this->db->where(COL_USERNAME, $ruser[COL_USERNAME])->update(TBL_USERS, $data);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      $ruser = $this->db
      ->where(COL_USERNAME,$ruser[COL_USERNAME])
      ->get(TBL_USERS)
      ->row_array();
      if(!empty($ruser)) {
        SetLoginSession($ruser);
      }

      ShowJsonSuccess('Pembaruan profil berhasil.');
      exit();
    } else {
      $this->load->view('site/user/profile', $data);
    }
  }
}
 ?>
