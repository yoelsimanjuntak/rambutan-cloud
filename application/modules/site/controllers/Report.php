<?php
class Report extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }
  }

  public function data() {
    $data['title'] = "Rekapitulasi Tiket";
    $this->template->load('adminlte', 'report/data', $data);
  }

  public function data_load(){
    $ruser = GetLoggedUser();
    $runit = array();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $runit = $this->db->where(COL_UNIQ, $ruser[COL_UNITID])->get(TBL_MUNIT)->row_array();
    }

    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    $IdOPD = !empty($_POST['idUnit'])?$_POST['idUnit']:null;
    $dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    $dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_CREATEDON,COL_TIKETNO,COL_TIKETNAMA,COL_TIKETNIK,COL_LAYANANNAMA,COL_REGIONNAMA);
    $cols = array(COL_TIKETNO,COL_TIKETNAMA,COL_TIKETNIK,COL_LAYANANNAMA,COL_REGIONNAMA);

    $queryAll = $this->db->get(TBL_TRTIKET);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      if($_POST['order']==COL_CREATEDON) $this->db->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, $_POST['order']['0']['dir']);
      else $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    if(!empty($dateFrom)) {
      $this->db->where('CAST('.TBL_TRTIKET.'.'.COL_CREATEDON.' as DATE) >= ', $dateFrom);
    }
    if(!empty($dateTo)) {
      $this->db->where('CAST('.TBL_TRTIKET.'.'.COL_CREATEDON.' as DATE) <= ', $dateTo);
    }
    if(!empty($IdOPD)) {
      $this->db->where('mregion.'.COL_UNIQ, $IdOPD);
    }

    $q = $this->db
    ->select('trtiket.*, mlayanan.LayananNama, mregion.RegionNama')
    ->join(TBL_MLAYANAN,TBL_MLAYANAN.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDLAYANAN,"left")
    ->join(TBL_MREGION,TBL_MREGION.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDREGION,"left")
    ->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, 'asc')
    ->get_compiled_select(TBL_TRTIKET, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('site/tiket/index/'.$r[COL_TIKETNO]).'" class="btn btn-xs btn-outline-info" target="_blank"><i class="fas fa-search"></i>&nbsp;LIHAT</a>',
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON])),
        $r[COL_TIKETNO],
        $r[COL_TIKETNAMA],
        //$r[COL_TIKETNIK],
        $r[COL_LAYANANNAMA],
        $r[COL_REGIONNAMA],
        !empty($r[COL_TIKETKEPUASAN])?$r[COL_TIKETKEPUASAN]:'-',
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function data_cetak() {
    if(!IsLogin()) {
      redirect('site/user/dashboard');
    }

    $data['title'] = 'Rekapitulasi Tiket';
    $this->load->view('report/data-cetak');
  }

  public function stat() {
    $data['title'] = "Statistik Tiket";
    $this->template->load('adminlte', 'report/stat', $data);
  }
}
?>
