<?php
class Admin extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }
  }

  public function tiket($status='') {
    $data['title'] = "Daftar Tiket";
    $data['status'] = $status;
    $this->template->load('adminlte', 'admin/tiket', $data);
  }

  public function tiket_load($status='') {
    $ruser = GetLoggedUser();
    $runit = array();
    if($ruser[COL_ROLEID]!=ROLEADMIN) {
      $runit = $this->db->where(COL_UNIQ, $ruser[COL_UNITID])->get(TBL_MUNIT)->row_array();
    }

    $start = $_POST['start'];
    $rowperpage = $_POST['length'];
    //$questStatus = !empty($_POST['filterStatus'])?$_POST['filterStatus']:null;
    //$dateFrom = !empty($_POST['dateFrom'])?$_POST['dateFrom']:date('Y-m-01');
    //$dateTo = !empty($_POST['dateTo'])?$_POST['dateTo']:date('Y-m-d');

    $orderdef = array(COL_CREATEDON=>'desc');
    $orderables = array(null,COL_CREATEDON,COL_TIKETNO,COL_TIKETNAMA,COL_TIKETNIK,COL_LAYANANNAMA,COL_REGIONNAMA);
    $cols = array(COL_TIKETNO,COL_TIKETNAMA,COL_TIKETNIK,COL_LAYANANNAMA,COL_REGIONNAMA);

    if(!empty($status)) {
      $this->db->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$status);
    }
    if($status==STATUS_BARU) {
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $this->db->where("trtiket.IdRegion = ".(!empty($runit)?$runit[COL_IDREGION]:'-999'));
      }
    } else {
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $this->db->where("(select IdUnit from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$ruser[COL_UNITID]);
      }
    }

    $queryAll = $this->db->get(TBL_TRTIKET);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      if($_POST['order']==COL_CREATEDON) $this->db->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, $_POST['order']['0']['dir']);
      else $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    if(!empty($status)) {
      $this->db->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$status);
    }
    if($status==STATUS_BARU) {
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $this->db->where("trtiket.IdRegion = ".(!empty($runit)?$runit[COL_IDREGION]:'-999'));
      }
    } else {
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        $this->db->where("(select IdUnit from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$ruser[COL_UNITID]);
      }
    }

    $q = $this->db
    ->select('trtiket.*, mlayanan.LayananNama, mregion.RegionNama, (select munit.UnitNama from trtiketstatus stat left join munit on munit.Uniq = stat.IdUnit where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) as Lokasi')
    ->join(TBL_MLAYANAN,TBL_MLAYANAN.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDLAYANAN,"left")
    ->join(TBL_MREGION,TBL_MREGION.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDREGION,"left")
    ->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, 'asc')
    ->get_compiled_select(TBL_TRTIKET, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $data[] = array(
        '<a href="'.site_url('site/admin/tiket-form/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-info"><i class="fas fa-search"></i>&nbsp;LIHAT</a>',
        date('Y-m-d H:i', strtotime($r[COL_CREATEDON])),
        $r[COL_TIKETNO],
        $r[COL_TIKETNAMA],
        //$r[COL_TIKETNIK],
        $r[COL_LAYANANNAMA],
        $r[COL_REGIONNAMA],
        !empty($r['Lokasi'])?$r['Lokasi']:'-',
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function tiket_form($id) {
    $data['title'] = "Tiket";
    $rdata = $this->db
    ->select('trtiket.*, mregion.RegionNama, mlayanan.LayananNama')
    ->join(TBL_MREGION,TBL_MREGION.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDREGION,"left")
    ->join(TBL_MLAYANAN,TBL_MLAYANAN.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDLAYANAN,"left")
    ->where(TBL_TRTIKET.'.'.COL_UNIQ, $id)
    ->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, 'desc')
    ->get(TBL_TRTIKET)
    ->row_array();

    if(empty($rdata)) {
      show_error('Parameter tidak valid!');
      exit();
    }

    $data['title'] = "Tiket No. ".$rdata[COL_TIKETNO];
    $data['data'] = $rdata;
    $this->template->load('adminlte', 'admin/tiket-form', $data);
  }

  public function tiket_change_stat($id, $stat) {
    $ruser = GetLoggedUser();
    $rdata = $this->db
    ->select('*,(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) as IdStatus')
    ->where(TBL_TRTIKET.'.'.COL_UNIQ, $id)
    ->get(TBL_TRTIKET)
    ->row_array();

    if(empty($rdata)) {
      echo 'Parameter tidak valid!';
      exit();
      /*ShowJsonError('Parameter tidak valid!');
      exit();*/
    }

    if(!empty($_POST)) {
      $rec = array(
        COL_IDTIKET=>$id,
        COL_IDSTATUS=>$stat,
        COL_IDUNIT=>$this->input->post(COL_IDUNIT),
        COL_STATUSKETERANGAN=>$this->input->post(COL_STATUSKETERANGAN),
        COL_CREATEDON=>date('Y-m-d H:i:s'),
        COL_CREATEDBY=>$ruser[COL_USERNAME]
      );

      $res = $this->db->insert(TBL_TRTIKETSTATUS, $rec);
      if(!$res) {
        $err = $this->db->error();
        ShowJsonError($err['message']);
        exit();
      }

      /*if($stat==STATUS_DISERAHKAN && !empty($_POST[COL_TIKETKEPUASAN])) {
        $res = $this->db->where(COL_UNIQ, $id)->update(TBL_TRTIKET, array(COL_TIKETKEPUASAN=>$this->input->post(COL_TIKETKEPUASAN)));
      }*/

      ShowJsonSuccess('Status tiket berhasil diubah.', array('redirect'=>site_url('site/admin/tiket/'.$rdata[COL_IDSTATUS])));
      exit();
    } else {
      $this->load->view('site/admin/tiket-form-status', array('data'=>$rdata, 'stat'=>$stat));
    }


  }
}
