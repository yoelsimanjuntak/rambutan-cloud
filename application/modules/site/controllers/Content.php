<?php
class Content extends MY_Controller {
  function __construct() {
    parent::__construct();
    if(!IsLogin()) {
      redirect('site/user/login');
    }
    if(GetLoggedUser()[COL_ROLEID] != ROLEADMIN) {
      redirect('site/user/dashboard');
    }
  }

  public function homepage() {
    $data['title'] = "Pengaturan Konten - Homepage";
    if(!empty($_POST)) {
      if(!empty($_POST['WelcomeText'])) {
        $rwelcome = $this->db
        ->where(COL_CONTENTTYPE,'WelcomeText')
        ->get(TBL_WEBCONTENT)
        ->row_array();
        if(empty($rwelcome)) {
          $res = $this->db
          ->insert(TBL_WEBCONTENT, array(COL_CONTENTTYPE=>'WelcomeText', COL_CONTENTTITLE=>'WelcomeText', COL_CONTENTDESC1=>$this->input->post('WelcomeText')));
        } else {
          $res = $this->db
          ->where(COL_CONTENTTYPE, 'WelcomeText')
          ->update(TBL_WEBCONTENT, array(COL_CONTENTDESC1=>$this->input->post('WelcomeText')));
        }
      }
      if(!empty($_POST['Email'])) {
        SetSetting('SETTING_ORG_MAIL', $this->input->post('Email'));
      }
      if(!empty($_POST['Phone1'])) {
        SetSetting('SETTING_ORG_PHONE', $this->input->post('Phone1'));
      }
      if(!empty($_POST['Phone2'])) {
        SetSetting('SETTING_ORG_FAX', $this->input->post('Phone2'));
      }
      if(!empty($_POST['Address'])) {
        SetSetting('SETTING_ORG_ADDRESS', $this->input->post('Address'));
      }
      if(!empty($_POST['GMaps'])) {
        SetSetting('SETTING_ORG_LAT', $this->input->post('GMaps'));
      }
      redirect(current_url());
    }
    $this->template->load('adminlte', 'content/homepage', $data);
  }

  public function testimoni() {
    $data['title'] = "Testimoni";
    $this->template->load('adminlte', 'content/testimoni', $data);
  }
  public function testimoni_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CONTENTTITLE=>'desc');
    $orderables = array(null,COL_CONTENTTITLE,COL_CONTENTDESC1,COL_CONTENTDESC2);
    $cols = array(COL_CONTENTTITLE,COL_CONTENTDESC1,COL_CONTENTDESC2);

    $queryAll = $this->db
    ->where(COL_CONTENTTYPE,'Testimonial')
    ->get(TBL_WEBCONTENT);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->where(COL_CONTENTTYPE,'Testimonial')
    ->get_compiled_select(TBL_WEBCONTENT, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/content/testimoni-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/content/testimoni-form/edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui"><i class="fas fa-pencil-alt"></i></a>&nbsp;';

      $data[] = array(
        $htmlBtn,
        $r[COL_CONTENTDESC1],
        $r[COL_CONTENTTITLE],
        (strlen($r[COL_CONTENTDESC2]) > 100 ? substr($r[COL_CONTENTDESC2], 0, 100) . "..." : $r[COL_CONTENTDESC2]),
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function testimoni_form($mode, $id=null) {
    $ruser = GetLoggedUser();

    if(!empty($_POST)) {
      if($mode=='add') {
        $this->db->trans_begin();
        try {
          $rec = array(
            COL_CONTENTTYPE=>'Testimonial',
            COL_CONTENTTITLE=>$this->input->post(COL_CONTENTTITLE),
            COL_CONTENTDESC1=>$this->input->post(COL_CONTENTDESC1),
            COL_CONTENTDESC2=>$this->input->post(COL_CONTENTDESC2),
          );

          $res = $this->db->insert(TBL_WEBCONTENT, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('SELESAI');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }  else if($mode=='edit') {
        if(empty($id)) {
          ShowJsonError('Parameter tidak valid!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_CONTENTTITLE=>$this->input->post(COL_CONTENTTITLE),
            COL_CONTENTDESC1=>$this->input->post(COL_CONTENTDESC1),
            COL_CONTENTDESC2=>$this->input->post(COL_CONTENTDESC2),
          );

          $res = $this->db->where(COL_UNIQ, $id)->update(TBL_WEBCONTENT, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('SELESAI');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      $data=array();
      if($mode=='edit') {
        $data['data'] = $this->db->where(COL_UNIQ, $id)->get(TBL_WEBCONTENT)->row_array();
      }
      $this->load->view('site/content/testimoni-form', $data);
    }
  }

  public function testimoni_delete($id) {
    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_WEBCONTENT);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }

  public function galeri() {
    $data['title'] = "Galeri";
    $this->template->load('adminlte', 'content/galeri', $data);
  }

  public function galeri_load() {
    $start = $_POST['start'];
    $rowperpage = $_POST['length'];

    $ruser = GetLoggedUser();
    $orderdef = array(COL_CONTENTTITLE=>'desc');
    $orderables = array(null,COL_CONTENTTITLE,COL_CONTENTDESC1,COL_CONTENTDESC2);
    $cols = array(COL_CONTENTTITLE,COL_CONTENTDESC1,COL_CONTENTDESC2);

    $queryAll = $this->db
    ->where(COL_CONTENTTYPE,'Galeri')
    ->get(TBL_WEBCONTENT);

    $i = 0;
    foreach($cols as $item){
      if(!empty($_POST['search']['value'])){
        if($i===0) {
          $this->db->group_start();
          $this->db->like($item, $_POST['search']['value']);
        } else {
          $this->db->or_like($item, $_POST['search']['value']);
        }
        if(count($cols) - 1 == $i){
          $this->db->group_end();
        }
      }
      $i++;
    }

    if(!empty($_POST['order'])){
      $this->db->order_by($orderables[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
    }else if(!empty($orderdef)){
      $order = $orderdef;
      $this->db->order_by(key($order), $order[key($order)]);
    }

    $q = $this->db
    ->where(COL_CONTENTTYPE,'Galeri')
    ->get_compiled_select(TBL_WEBCONTENT, FALSE);
    $rec = $this->db->query($q." LIMIT $rowperpage OFFSET $start");
    $data = [];

    foreach($rec->result_array() as $r) {
      $htmlBtn = '';
      $htmlBtn .= '<a href="'.site_url('site/content/galeri-delete/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-danger btn-action" data-toggle="tooltip" data-placement="top" data-title="Hapus"><i class="fas fa-trash"></i></a>&nbsp;';
      $htmlBtn .= '<a href="'.site_url('site/content/galeri-form/edit/'.$r[COL_UNIQ]).'" class="btn btn-xs btn-outline-primary btn-edit" data-toggle="tooltip" data-placement="top" data-title="Perbarui"><i class="fas fa-pencil-alt"></i></a>&nbsp;';

      $data[] = array(
        $htmlBtn,
        $r[COL_CONTENTTITLE],
        (strlen($r[COL_CONTENTDESC1]) > 100 ? substr($r[COL_CONTENTDESC1], 0, 100) . "..." : $r[COL_CONTENTDESC1])
      );
    }

    $result = array(
      "draw" => $_POST['draw'],
      "recordsFiltered" => $this->db->query($q)->num_rows(),
      "recordsTotal" => $queryAll->num_rows(),
      "data" => $data,
    );

    echo json_encode($result);
    exit();
  }

  public function galeri_form($mode, $id=null) {
    $ruser = GetLoggedUser();

    if(!empty($_POST)) {
      $config['upload_path'] = './assets/media/upload/';
      $config['allowed_types'] = "jpg|jpeg|png|";
      $config['max_size']	= 5120;
      $config['max_width']  = 4000;
      $config['max_height']  = 4000;
      $config['overwrite'] = FALSE;

      $this->load->library('upload',$config);

      if($mode=='add') {
        $this->db->trans_begin();
        try {
          $rec = array(
            COL_CONTENTTYPE=>'Galeri',
            COL_CONTENTTITLE=>$this->input->post(COL_CONTENTTITLE),
            COL_CONTENTDESC1=>$this->input->post(COL_CONTENTDESC1),
          );

          if(!empty($_FILES)) {
            $res = $this->upload->do_upload('file');
            if(!$res) {
              $err = $this->upload->display_errors('', '');
              throw new Exception($err);
            }
            $upl = $this->upload->data();
            $rec[COL_CONTENTDESC2] = $upl['file_name'];
          }

          $res = $this->db->insert(TBL_WEBCONTENT, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('SELESAI');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }  else if($mode=='edit') {
        if(empty($id)) {
          ShowJsonError('Parameter tidak valid!');
          exit();
        }

        $this->db->trans_begin();
        try {
          $rec = array(
            COL_CONTENTTITLE=>$this->input->post(COL_CONTENTTITLE),
            COL_CONTENTDESC1=>$this->input->post(COL_CONTENTDESC1)
          );

          $res = $this->db->where(COL_UNIQ, $id)->update(TBL_WEBCONTENT, $rec);
          if(!$res) {
            $err = $this->db->error();
            throw new Exception($err['message']);
          }

          $this->db->trans_commit();
          ShowJsonSuccess('SELESAI');
          exit();
        } catch(Exception $ex) {
          $this->db->trans_rollback();
          ShowJsonError($ex->getMessage());
          exit();
        }
      }
    } else {
      $data=array();
      if($mode=='edit') {
        $data['data'] = $this->db->where(COL_UNIQ, $id)->get(TBL_WEBCONTENT)->row_array();
      }
      $this->load->view('site/content/galeri-form', $data);
    }
  }

  public function galeri_delete($id) {
    $rdata = $this->db->where(COL_UNIQ, $id)->get(TBL_WEBCONTENT)->row_array();
    if(empty($rdata)) {
      ShowJsonError('Parameter tidak valid!');
      exit();
    }

    $res = $this->db->where(COL_UNIQ, $id)->delete(TBL_WEBCONTENT);
    if(!$res) {
      $err = $this->db->error();
      ShowJsonError($err['message']);
      exit();
    }

    if(file_exists('./assets/media/upload/'.$rdata[COL_CONTENTDESC2])) {
      unlink('./assets/media/upload/'.$rdata[COL_CONTENTDESC2]);
    }

    ShowJsonSuccess('BERHASIL DIHAPUS');
  }
}
?>
