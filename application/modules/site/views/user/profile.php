<form id="form-profile" method="post" action="#">
  <div class="modal-header">
    <h5 class="modal-title">Profil</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true"><i class="fa fa-close"></i></span>
    </button>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-sm-8">
        <div class="form-group">
          <label>Nama Lengkap</label>
          <input type="text" class="form-control" name="<?=COL_FULLNAME?>" value="<?=!empty($data)?$data[COL_FULLNAME]:''?>" required />
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label>Jenis Kelamin</label>
          <select class="form-control" name="<?=COL_GENDER?>" style="width: 100%" required>
            <option value="MALE">LAKI-LAKI</option>
            <option value="FEMALE">PEREMPUAN</option>
          </select>
        </div>
      </div>
      <div class="col-sm-8">
        <div class="form-group">
          <label>No. HP / WA</label>
          <input type="text" class="form-control" name="<?=COL_PHONE?>" value="<?=!empty($data)?$data[COL_PHONE]:''?>" />
        </div>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label>Tgl. Lahir</label>
          <input type="text" class="form-control datepicker text-right" name="<?=COL_DATEBIRTH?>" value="<?=!empty($data)?$data[COL_DATEBIRTH]:''?>" />
        </div>
      </div>

    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
    <button type="submit" class="btn btn-sm btn-primary btn-ok"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
  </div>
</form>
