<?php
$ruser = GetLoggedUser();
$runit = array();
if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $runit = $this->db->where(COL_UNIQ, $ruser[COL_UNITID])->get(TBL_MUNIT)->row_array();
}
if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where("trtiket.IdRegion = ".(!empty($runit)?$runit[COL_IDREGION]:'-999'));
}
$rtiketBaru_ = $this->db
->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".STATUS_BARU)
->count_all_results(TBL_TRTIKET);

if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where("trtiket.IdRegion = ".(!empty($runit)?$runit[COL_IDREGION]:'-999'));
}
$rtiketBaru = $this->db
->select('trtiket.*, (select stat.CreatedOn from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) as Timestamp')
->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".STATUS_BARU)
->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, 'desc')
->get(TBL_TRTIKET)
->row_array();

if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where("(select IdUnit from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$ruser[COL_UNITID]);
}
$rtiketProses_ = $this->db
->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".STATUS_PROSES)
->count_all_results(TBL_TRTIKET);

if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where("(select IdUnit from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$ruser[COL_UNITID]);
}
$rtiketProses = $this->db
->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".STATUS_PROSES)
->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, 'desc')
->get(TBL_TRTIKET)
->row_array();

if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where("(select IdUnit from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$ruser[COL_UNITID]);
}
$rtiketSelesai_ = $this->db
->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".STATUS_SELESAI)
->count_all_results(TBL_TRTIKET);

if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where("(select IdUnit from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$ruser[COL_UNITID]);
}
$rtiketSelesai = $this->db
->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".STATUS_SELESAI)
->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, 'desc')
->get(TBL_TRTIKET)
->row_array();

if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where("(select IdUnit from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$ruser[COL_UNITID]);
}
$rtiketDiserahkan_ = $this->db
->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".STATUS_DISERAHKAN)
->count_all_results(TBL_TRTIKET);

if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where("(select IdUnit from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$ruser[COL_UNITID]);
}
$rtiketDiserahkan = $this->db
->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".STATUS_DISERAHKAN)
->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, 'desc')
->get(TBL_TRTIKET)
->row_array();

if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where("(select IdUnit from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$ruser[COL_UNITID]);
}
$rtiketDitolak_ = $this->db
->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".STATUS_DITOLAK)
->count_all_results(TBL_TRTIKET);

if($ruser[COL_ROLEID]!=ROLEADMIN) {
  $this->db->where("(select IdUnit from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".$ruser[COL_UNITID]);
}
$rtiketDitolak = $this->db
->where("(select IdStatus from trtiketstatus stat where stat.IdTiket = trtiket.Uniq order by stat.CreatedOn desc limit 1) = ".STATUS_DITOLAK)
->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, 'desc')
->get(TBL_TRTIKET)
->row_array();
?>
 <style>
 th {
   border-right-width: 1px !important;
 }
 .table thead tr:first-child td {
   border-bottom: none !important;
}
.small-box .inner {
  padding-bottom: 40px !important;
}
 </style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row align-items-stretch">
      <div class="col-sm-3 d-flex align-items-stretch">
        <div class="small-box bg-secondary w-100">
          <div class="inner">
            <h3><?=number_format($rtiketBaru_)?></h3>

            <p>
              BARU
              <?=!empty($rtiketBaru)?'<br /><small class="font-italic">Data terakhir pada <strong>'.date('d M Y H:i', strtotime($rtiketBaru['Timestamp'])).'</strong></small>':''?>
            </p>
          </div>
          <div class="icon">
            <i class="far fa-inbox"></i>
          </div>
          <a href="<?=site_url('site/admin/tiket/'.STATUS_BARU)?>" class="small-box-footer" style="position: absolute !important; bottom: 0 !important; width: 100% !important">LIHAT <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-sm-3 d-flex align-items-stretch">
        <div class="small-box bg-primary w-100">
          <div class="inner">
            <h3><?=number_format($rtiketProses_)?></h3>

            <p>
              PROSES
              <?=!empty($rtiketProses)?'<br /><small class="font-italic">Data terakhir pada <strong>'.date('d M Y H:i', strtotime($rtiketProses[COL_CREATEDON])).'</strong></small>':''?>
            </p>
          </div>
          <div class="icon">
            <i class="far fa-sync"></i>
          </div>
          <a href="<?=site_url('site/admin/tiket/'.STATUS_PROSES)?>" class="small-box-footer" style="position: absolute !important; bottom: 0 !important; width: 100% !important">LIHAT <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-sm-3 d-flex align-items-stretch">
        <div class="small-box bg-success w-100">
          <div class="inner">
            <h3><?=number_format($rtiketSelesai_)?></h3>

            <p>
              SELESAI
              <?=!empty($rtiketSelesai)?'<br /><small class="font-italic">Data terakhir pada <strong>'.date('d M Y H:i', strtotime($rtiketSelesai[COL_CREATEDON])).'</strong></small>':''?>
            </p>
          </div>
          <div class="icon">
            <i class="far fa-check-circle"></i>
          </div>
          <a href="<?=site_url('site/admin/tiket/'.STATUS_SELESAI)?>" class="small-box-footer" style="position: absolute !important; bottom: 0 !important; width: 100% !important">LIHAT <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-sm-3 d-flex align-items-stretch">
        <div class="small-box bg-info w-100">
          <div class="inner">
            <h3><?=number_format($rtiketDiserahkan_)?></h3>

            <p>
              DISERAHKAN
              <?=!empty($rtiketDiserahkan)?'<br /><small class="font-italic">Data terakhir pada <strong>'.date('d M Y H:i', strtotime($rtiketDiserahkan[COL_CREATEDON])).'</strong></small>':''?>
            </p>
          </div>
          <div class="icon">
            <i class="far fa-user-check"></i>
          </div>
          <a href="<?=site_url('site/admin/tiket/'.STATUS_DISERAHKAN)?>" class="small-box-footer" style="position: absolute !important; bottom: 0 !important; width: 100% !important">LIHAT <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
      <div class="col-sm-3 d-flex align-items-stretch">
        <div class="small-box bg-danger w-100">
          <div class="inner">
            <h3><?=number_format($rtiketDitolak_)?></h3>

            <p>
              DITOLAK
              <?=!empty($rtiketDitolak)?'<br /><small class="font-italic">Data terakhir pada <strong>'.date('d M Y H:i', strtotime($rtiketDitolak[COL_CREATEDON])).'</strong></small>':''?>
            </p>
          </div>
          <div class="icon">
            <i class="far fa-times-circle"></i>
          </div>
          <a href="<?=site_url('site/admin/tiket/'.STATUS_DITOLAK)?>" class="small-box-footer" style="position: absolute !important; bottom: 0 !important; width: 100% !important">LIHAT <i class="fas fa-arrow-circle-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>
<script type="text/javascript">
</script>
