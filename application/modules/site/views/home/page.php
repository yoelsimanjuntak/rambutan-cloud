<?php
$rheader = $this->db
->where(COL_ISHEADER, 1)
->where(COL_POSTID, $data[COL_POSTID])
->get(TBL__POSTIMAGES)
->result_array();

$postContent = $data[COL_POSTCONTENT];
$rimg = $this->db
->where(COL_ISHEADER.' != ', 1)
->where(COL_POSTID, $data[COL_POSTID])
->get(TBL__POSTIMAGES)
->result_array();
foreach($rimg as $img) {
  if(!empty($img[COL_IMGSHORTCODE])) {
    $postContent = str_replace($img[COL_IMGSHORTCODE], '<img src="'.MY_UPLOADURL.$img[COL_IMGPATH].'" style="max-width: 100%" /><p style="margin-top: 10px !important; font-size: 10px; font-style:italic; line-height: 1.5 !important">'.$img[COL_IMGDESC].'</p>', $postContent);
  }
}

$arrTags = array();
if(!empty($data[COL_POSTMETATAGS])) {
  $arrTags = explode(",", $data[COL_POSTMETATAGS]);
}

$txtShareWA = 'Jangan lewatkan update berita dan informasi terbaru dari '.$this->setting_web_name.' | '.urlencode(current_url());
?>
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="card mb-3">
        <div class="card-body">
          <h2 class="card-title"><?=$data[COL_POSTTITLE]?></h2>
          <div class="meta">
            <div class="meta-left">
              <span class="author"><i class="far fa-user"></i>&nbsp;<?=$data[COL_FULLNAME]?></span>&nbsp;
              <span class="author pl-2"><i class="far fa-clock"></i>&nbsp;<?=date('d-m-Y H:i', strtotime($data[COL_CREATEDON]))?></span>&nbsp;
              <span class="views pl-2"><i class="fa fa-eye"></i>&nbsp;<?=number_format($data[COL_TOTALVIEW])?> kali dilihat</span>
            </div>
          </div>
          <div class="news-head mt-3">
            <?php
            if(!empty($rheader)) {
              if(count($rheader) > 1) {
                ?>
                <div class="row mb-2">
                  <?php
                  foreach($rheader as $f) {
                    if(strpos(mime_content_type(MY_UPLOADPATH.$f[COL_IMGPATH]), 'image') !== false) {
                      ?>
                      <div class="col-12 col-sm-6 col-md-6 d-flex align-items-stretch p-2">
                        <div href="<?=MY_UPLOADURL.$f[COL_IMGPATH]?>"
                        data-toggle="lightbox"
                        data-title="<?=$data[COL_POSTTITLE]?>"
                        data-gallery="gallery"
                        style="background: url('<?=MY_UPLOADURL.$f[COL_IMGPATH]?>');
                                background-size: cover;
                                background-repeat: no-repeat;
                                background-position: center;
                                width: 100%;
                                min-height: 600px;
                                cursor: pointer;">
                        </div>
                      </div>
                      <?php
                    } else {
                      ?>
                      <div class="col-12 col-sm-12 col-md-12 mb-3 d-flex align-items-stretch">
                        <embed src="<?=MY_UPLOADURL.$f[COL_IMGPATH]?>" width="100%" height="600" />
                      </div>
                      <?php
                    }
                    ?>
                  <?php
                  }
                  ?>
                </div>
                <?php
              } else {
                ?>
                <div class="row mb-2">
                  <div class="col-12 text-center">
                    <div href="<?=MY_UPLOADURL.$rheader[0][COL_IMGPATH]?>"
                    data-toggle="lightbox"
                    data-title="<?=$data[COL_POSTTITLE]?>"
                    data-gallery="gallery"
                    style="background: url('<?=MY_UPLOADURL.$rheader[0][COL_IMGPATH]?>');
                            background-size: cover;
                            background-repeat: no-repeat;
                            background-position: center;
                            width: 100%;
                            min-height: 400px;
                            cursor: pointer;">
                    </div>
                  </div>
                </div>
                <?php
              }
            }
            ?>
          </div>
          <div class="news-text mt-5"><?=$postContent?></div>
        </div>
      </div>
      <div class="card">
        <div class="card-body">
          <div class="comments-form">
            <div id="disqus_thread"></div>
            <script>

                /**
                 *  RECOMMENDED CONFIGURATION VARIABLES: EDIT AND UNCOMMENT THE SECTION BELOW TO INSERT DYNAMIC VALUES FROM YOUR PLATFORM OR CMS.
                 *  LEARN WHY DEFINING THESE VARIABLES IS IMPORTANT: https://disqus.com/admin/universalcode/#configuration-variables*/
                /*
                 */
                var disqus_config = function () {
                    this.page.url = '<?=site_url('site/home/page/'.$data[COL_POSTSLUG])?>';  // Replace PAGE_URL with your page's canonical URL variable
                    this.page.identifier = '<?=$data[COL_POSTSLUG]?>'; // Replace PAGE_IDENTIFIER with your page's unique identifier variable
                };
                (function() { // DON'T EDIT BELOW THIS LINE
                    var d = document, s = d.createElement('script');
                    s.src = '<?=$this->setting_web_disqus_url?>';
                    s.setAttribute('data-timestamp', +new Date());
                    (d.head || d.body).appendChild(s);
                })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
