
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Kecamatan Rambutan Pemerintah Kota Tebing Tinggi</title>
    <link rel="icon" type="image/png" href="<?=base_url().$this->setting_web_icon?>">
    <meta name="description" content="Website Resmi Kecamatan Rambutan Pemerintah Kota Tebing Tinggi">
		<meta name="author" content="Partopi Tao">
		<meta name="keyword" content="kecamatan, rambutan, tebing, tebing tinggi, website, pemerintah">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="Website Resmi Kecamatan Rambutan" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?=base_url()?>" />
		<meta property="og:image" content="<?=base_url().$this->setting_web_logo?>" />

    <!-- CSS Dependencies -->
    <link href="<?=base_url()?>assets/themes/shards/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/shards/dist/css/shards.min.css?v=3.0.0">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/shards/dist/css/shards-extras.min.css?version=3.0.0">
    <script src="<?=base_url()?>assets/themes/shards/dist/js/jquery-3.2.1.min.js"></script>
    <style>
    html {
      scroll-behavior: smooth;
    }
    .section-title:after {
      background: #00b8d8 !important;
    }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?=base_url().$this->setting_web_preloader?>) center no-repeat #fff;
    }
    </style>
    <script>
    $(window).on('load', function(){
      $(".se-pre-con").fadeOut("slow");
    });
    </script>
</head>
<?php
$rsliderImg = $this->db->where('ContentType', 'Slider')->where('ContentTitle', 'ImgCarousel')->get(TBL_WEBCONTENT)->row_array();
$rprofileCamat = $this->db->where('ContentType', 'Profile')->where('ContentTitle', 'TxtCamat')->get(TBL_WEBCONTENT)->row_array();
$rprofilePenduduk = $this->db->where('ContentType', 'Profile')->where('ContentTitle', 'TxtPenduduk')->get(TBL_WEBCONTENT)->row_array();
$rprofileLuas = $this->db->where('ContentType', 'Profile')->where('ContentTitle', 'TxtLuas')->get(TBL_WEBCONTENT)->row_array();
$rprofileKelurahan = $this->db->where('ContentType', 'Profile')->where('ContentTitle', 'TxtKelurahan')->get(TBL_WEBCONTENT)->row_array();

$rberita = $this->db
->where(COL_POSTCATEGORYID,1)
->where(COL_ISSUSPEND, 0)
->order_by(COL_POSTDATE, 'desc')
->get(TBL__POSTS, 0, 6)
->result_array();

$rinfografis = $this->db
->where(COL_POSTCATEGORYID,2)
->where(COL_ISSUSPEND, 0)
->order_by(COL_POSTDATE, 'desc')
->get(TBL__POSTS, 0, 6)
->result_array();
?>
<body class="shards-app-promo-page--1">
  <div class="se-pre-con"></div>
  <div class="welcome d-flex justify-content-center flex-column" style="background: url('<?=MY_IMAGEURL.'img-banner.jpg'?>') no-repeat center center fixed !important; background-size: cover !important">
    <div class="container">
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark pt-4 px-0">
        <a class="navbar-brand" href="#">
          <img src="<?=base_url().$this->setting_web_icon?>" class="float-left d-block" alt="Pemerintah Kota Tebing Tinggi">
          <span style="line-height: 1; display: block; margin-left: 2.5rem; padding-top: .1rem !important">Kecamatan Rambutan<br /><small>Pemerintah Kota Tebing Tinggi</small></span>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="<?=base_url()?>">Beranda <span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#section-profile">Profil</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#section-blog">Berita</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#section-info">Infografis</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="#section-contact">Kontak</a>
            </li>
          </ul>
        </div>
      </nav>
      <!-- / Navigation -->
    </div> <!-- .container -->

    <!-- Inner Wrapper -->
    <div class="inner-wrapper mt-auto mb-auto container">
      <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-12 mt-auto mb-auto">
            <h3 class="welcome-heading display-5 text-white slide-in">Website Resmi<br />Kecamatan Rambutan</h3>

            <p class="text-muted mt-4">Silakan kunjungi aplikasi <strong class="text-white"><?=$this->setting_web_name?></strong> untuk mendapatkan akses layanan publik secara daring.</p>
            <a href="<?=site_url('site/ticket')?>" class="btn btn-info btn-pill align-self-center mb-4">Buka Aplikasi <?=$this->setting_web_name?>&nbsp;<i class="fa fa-arrow-circle-right mr-2"></i></a>
        </div>

        <?php
        if(!empty($rsliderImg)) {
          $arrimg = explode(",", $rsliderImg[COL_CONTENTDESC1]);
          if(count($arrimg)>0) {
            ?>
            <div class="col-lg-6 col-md-6 col-sm-12 ml-auto">
              <div id="carouselExampleIndicators" class="carousel slide elevation-2" data-ride="carousel">
                <ol class="carousel-indicators">
                  <?php
                  $i=0;
                  foreach($arrimg as $img) {
                    ?>
                    <li data-target="#carouselExampleIndicators" data-slide-to="<?=$i?>" class="<?=$i==0?'active':''?>"></li>
                    <?php
                    $i++;
                  }
                  ?>
                </ol>
                <div class="carousel-inner" style="border-radius: 2% !important">
                  <?php
                  $i=0;
                  foreach($arrimg as $img) {
                    ?>
                    <div class="carousel-item <?=$i==0?'active':''?>">
                      <img class="d-block w-100" src="<?=MY_IMAGEURL.'slide/'.$img?>">
                    </div>
                    <?php
                    $i++;
                  }
                  ?>
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                </a>
              </div>
            </div>
            <?php
          }
        }
        ?>

      </div>
    </div>
    <!-- / Inner Wrapper -->
  </div>
  <!-- Our Services Section -->
  <div id="app-features" class="app-features section">
    <div class="container-fluid">
      <div class="row">
        <div class="app-screenshot col-lg-5 col-md-12 col-sm-12 px-0 py-5" style="background: url('<?=MY_IMAGEURL.'img-banner.jpg'?>') no-repeat center center fixed !important; background-size: cover !important">
        </div>

        <!-- App Features Wrapper -->
        <div class="app-features-wrapper col-lg-7 col-md-6 col-sm-12 pl-5 py-3 mx-auto" id="section-profile">
          <div class="container">
            <h3 class="section-title underline--left my-5">Profil Kecamatan</h3>
            <div class="feature py-4 d-flex">
              <div class="icon text-white mr-5" style="border-radius: 0 !important; box-shadow: none !important; background: url('<?=MY_IMAGEURL.'img-camat.jpg'?>'); background-size: cover !important; min-width: 100px; height: 100px"></div>
              <div>
                  <h5>Camat</h5>
                  <p><?=!empty($rprofileCamat)?$rprofileCamat[COL_CONTENTDESC1]:'-'?><br /><small>NIP. <?=!empty($rprofileCamat)?$rprofileCamat[COL_CONTENTDESC2]:'-'?></small></p>
              </div>
            </div>
            <div class="feature py-4 d-flex">
              <div class="icon text-white mr-5" style="border-radius: 0 !important; box-shadow: none !important; background: url('<?=MY_IMAGEURL.'img-people.png'?>'); background-size: cover !important; min-width: 100px; height: 100px"></div>
              <div>
                  <h5>Jumlah Penduduk</h5>
                  <p><?=!empty($rprofilePenduduk)?$rprofilePenduduk[COL_CONTENTDESC1]:'-'?><br /><small><?=!empty($rprofilePenduduk)?$rprofilePenduduk[COL_CONTENTDESC2]:'-'?></small></p>
              </div>
            </div>
            <div class="feature py-4 d-flex">
              <div class="icon text-white mr-5" style="border-radius: 0 !important; box-shadow: none !important; background: url('<?=MY_IMAGEURL.'img-map.png'?>'); background-size: cover !important; min-width: 100px; height: 100px"></div>
              <div>
                  <h5>Luas Wilayah</h5>
                  <p><?=!empty($rprofileLuas)?$rprofileLuas[COL_CONTENTDESC1]:'-'?><br /><small><?=!empty($rprofileLuas)?$rprofileLuas[COL_CONTENTDESC2]:'-'?></small></p>
              </div>
            </div>
            <div class="feature py-4 d-flex">
              <div class="icon text-white mr-5" style="border-radius: 0 !important; box-shadow: none !important; background: url('<?=MY_IMAGEURL.'img-kelurahan.png'?>'); background-size: cover !important; min-width: 100px; height: 100px"></div>
              <div>
                  <h5>Kelurahan</h5>
                  <?php
                  if(empty($rprofileKelurahan)) {
                    echo '<p>-</p>';
                  } else {
                    $rkelurahan = explode(",", $rprofileKelurahan[COL_CONTENTDESC1]);
                    echo '<ul>';
                    foreach($rkelurahan as $kel) {
                      echo '<li>'.$kel.'</li>';
                    }
                    echo '</ul>';
                  }
                  ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- / Our Services Section -->

  <!-- Our Blog Section -->
  <div class="blog section section-invert py-4" id="section-blog">
    <h3 class="section-title text-center m-5">Berita</h3>

    <div class="container">
      <div class="py-4">
        <div class="row">
          <?php
          if(!empty($rberita)) {
            ?>
            <div class="card-deck">
              <?php
              foreach($rberita as $b) {
                $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
                $rthumb = $this->db
                ->where(COL_POSTID, $b[COL_POSTID])
                ->where(COL_ISTHUMBNAIL, 1)
                ->get(TBL__POSTIMAGES)
                ->row_array();
                ?>
                <div class="col-md-12 col-lg-4">
                  <div class="card mb-4 w-100">
                    <div style="
                    height: 250px;
                    width: 100%;
                    background-image: url('<?=!empty($rthumb)&&file_exists(MY_UPLOADPATH.$rthumb[COL_IMGPATH])?MY_UPLOADURL.$rthumb[COL_IMGPATH]:MY_IMAGEURL.'img-default.jpeg'?>');
                    background-size: cover;
                    background-repeat: no-repeat;
                    background-position: center;
                    border-top-left-radius: .625rem;
                    border-top-right-radius: .625rem;
                    ">
                    </div>
                    <div class="card-body">
                      <h4 class="card-title"><?=$b[COL_POSTTITLE]?></h4>
                      <p class="card-text"><?=strlen($strippedcontent) > 150 ? substr($strippedcontent, 0, 150) . "..." : $strippedcontent ?></p>
                      <a class="btn btn-info btn-pill" href="<?=site_url('site/home/page/'.$b[COL_POSTSLUG])?>">BACA SELENGKAPNYA <i class="far fa-arrow-circle-right"></i></a>
                    </div>
                  </div>
                </div>
                <?php
              }
              ?>
            </div>

            <?php
          } else {
            ?>
            <div class="col-12">
              <p class="text-center font-italic">Maaf, data belum tersedia untuk saat ini.</p>
            </div>

            <?php
          }
          ?>
        </div>
      </div>
    </div>
  </div>
  <!-- / Our Blog Section -->

  <!-- Testimonials Section -->
  <div class="blog section py-4" id="section-info">
      <h3 class="section-title text-center m-5">Infografis</h3>
      <div class="container py-5">
        <div class="row">
          <?php
          if(!empty($rinfografis)) {
            ?>
            <?php
            foreach($rinfografis as $b) {
              $strippedcontent = strip_tags($b[COL_POSTCONTENT]);
              $rthumb = $this->db
              ->where(COL_POSTID, $b[COL_POSTID])
              ->where(COL_ISTHUMBNAIL, 1)
              ->get(TBL__POSTIMAGES)
              ->row_array();
              ?>
              <div class="col-md-12 col-lg-4">
                <div class="mb-2" style="
                height: 250px;
                width: 100%;
                background-image: url('<?=!empty($rthumb)&&file_exists(MY_UPLOADPATH.$rthumb[COL_IMGPATH])?MY_UPLOADURL.$rthumb[COL_IMGPATH]:MY_IMAGEURL.'img-default.jpeg'?>');
                background-size: cover;
                background-repeat: no-repeat;
                background-position: center;
                border-radius: .625rem;
                "></div>
                <p class="text-center text-muted">
                  <?=$b[COL_POSTTITLE]?>
                </p>
              </div>
              <?php
            }
            ?>
            <?php
          } else {
            ?>
            <div class="col-12">
              <p class="text-center font-italic">Maaf, data belum tersedia untuk saat ini.</p>
            </div>

            <?php
          }
          ?>
        </div>
      </div>
  </div>
  <!-- / Testimonials Section -->

  <!-- Contact Section -->
  <div class="contact section-invert py-4" id="section-contact">
    <h3 class="section-title text-center m-5">Kontak</h3>
    <div class="container py-4">
      <div class="row justify-content-md-center px-4">
        <div class="contact-form col-sm-12 col-md-12 col-lg-12 p-4 mb-4 card">
          <div class="row">
            <div class="col-sm-8 col-md-8 col-lg-8">
              <?=$this->setting_org_lat?>
            </div>
            <div class="col-sm-4 col-md-4 col-lg-4">
              <div class="feature py-4 d-flex">
                <div class="icon text-white bg-info mr-3" style="min-width: 40px !important; width: 40px !important; height: 40px !important; line-height: 40px !important; font-size: 18px !important"><i class="fa fa-building"></i></div>
                <div>
                    <h5>ALAMAT</h5>
                    <p><?=$this->setting_org_address?></p>
                </div>
              </div>
              <div class="feature py-4 d-flex">
                <div class="icon text-white bg-info mr-3" style="min-width: 40px !important; width: 40px !important; height: 40px !important; line-height: 40px !important; font-size: 18px !important"><i class="fa fa-phone"></i></div>
                <div>
                    <h5>NO. TELP</h5>
                    <p><?=$this->setting_org_phone?></p>
                </div>
              </div>
              <div class="feature py-4 d-flex">
                <div class="icon text-white bg-info mr-3" style="min-width: 40px !important; width: 40px !important; height: 40px !important; line-height: 40px !important; font-size: 18px !important"><i class="fa fa-envelope"></i></div>
                <div>
                    <h5>EMAIL</h5>
                    <p><?=$this->setting_org_mail?></p>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  <!-- / Contact Section -->

  <!-- Footer Section -->
  <footer>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
      <div class="container">
        <a class="navbar-brand" href="<?=base_url()?>">&copy; 2022</a>
        <span class="right text-white">Partopi Tao</span>
      </div>
    </nav>
  </footer>
  <!-- / Footer Section -->
  <!-- JavaScript Dependencies -->
<script async defer src="<?=base_url()?>assets/themes/shards/dist/js/buttons.js"></script>
<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
<script src="<?=base_url()?>assets/themes/shards/dist/js/popper.min.js"></script>
<script src="<?=base_url()?>assets/themes/shards/dist/js/bootstrap.min.js"></script>
<script src="<?=base_url()?>assets/themes/shards/dist/js/shards.min.js"></script>
</body>
</html>
