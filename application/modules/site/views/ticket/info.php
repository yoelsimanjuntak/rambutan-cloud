<?php
$txtStatus = '-';
$rstatus = $this->db
->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_UNIQ." = ".TBL_TRTIKETSTATUS.".".COL_IDSTATUS,"left")
->join(TBL_MUNIT,TBL_MUNIT.'.'.COL_UNIQ." = ".TBL_TRTIKETSTATUS.".".COL_IDUNIT,"left")
->where(COL_IDTIKET, $res[COL_UNIQ])
->order_by(TBL_TRTIKETSTATUS.'.'.COL_CREATEDON, 'desc')
->get(TBL_TRTIKETSTATUS)
->row_array();

if(!empty($rstatus)) {
  if($rstatus[COL_IDSTATUS]==STATUS_BARU) $txtStatus = '<span class="badge badge-secondary">'.$rstatus[COL_STATUSNAMA].'</span>';
  else if($rstatus[COL_IDSTATUS]==STATUS_PROSES) $txtStatus = '<span class="badge badge-primary">'.$rstatus[COL_STATUSNAMA].'</span>';
  else if($rstatus[COL_IDSTATUS]==STATUS_SELESAI) $txtStatus = '<span class="badge badge-success">'.$rstatus[COL_STATUSNAMA].'</span>';
  else if($rstatus[COL_IDSTATUS]==STATUS_DISERAHKAN) $txtStatus = '<span class="badge badge-info">'.$rstatus[COL_STATUSNAMA].'</span>';
  else if($rstatus[COL_IDSTATUS]==STATUS_DITOLAK) $txtStatus = '<span class="badge badge-danger">'.$rstatus[COL_STATUSNAMA].'</span>';
}
?>
<div class="row mt-4">
  <div class="col-sm-6">
    <table class="table table-striped" style="border: 1px solid #dedede">
      <tr>
        <td style="vertical-align: top; width: 100px; white-space: nowrap">Status</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
        <td><?=$txtStatus?></td>
      </tr>
      <?php
      if(!empty($rstatus)&&($rstatus[COL_IDSTATUS]==STATUS_SELESAI || $rstatus[COL_IDSTATUS]==STATUS_DISERAHKAN)) {
        ?>
        <tr>
          <td style="vertical-align: top;">Tanggapan</td>
          <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
          <td style="white-space: nowrap">
            <?php
            if(empty($res[COL_TIKETKEPUASAN])) {
              ?>
              <div class="row">
                <div class="col-sm-6">
                  <a class="btn btn-block btn-rate mb-2 btn-outline-primary btn-sm" href="<?=site_url('site/ticket/rate/'.$res[COL_UNIQ].'/puas')?>" data-rating="PUAS"><i class="far fa-thumbs-up"></i>&nbsp;PUAS</a>
                </div>
                <div class="col-sm-6">
                  <a class="btn btn-block btn-rate mb-2 btn-outline-danger btn-sm" href="<?=site_url('site/ticket/rate/'.$res[COL_UNIQ].'/kurang-puas')?>" data-rating="KURANG PUAS"><i class="far fa-thumbs-down"></i>&nbsp;KURANG PUAS</a>
                </div>
              </div>
              <?php
            } else {
              ?>
              <strong class="<?=$res[COL_TIKETKEPUASAN]=='PUAS'?'text-primary':'text-danger'?>"><?=$res[COL_TIKETKEPUASAN]?></strong>
              <?php
            }
            ?>
          </td>
        </tr>
        <?php
      }
      ?>
      <?php
      if(!empty($rstatus)&&!empty($rstatus[COL_UNITNAMA])) {
        ?>
        <tr>
          <td style="vertical-align: top; width: 100px; white-space: nowrap">Lokasi</td>
          <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
          <td><strong><?=strtoupper($rstatus[COL_UNITNAMA])?></strong></td>
        </tr>
        <?php
      }
      ?>
      <tr>
        <td style="vertical-align: top; width: 100px; white-space: nowrap">No. Tiket</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
        <td><strong><?=$res[COL_TIKETNO]?></strong></td>
      </tr>
      <tr>
        <td style="vertical-align: top; width: 100px; white-space: nowrap">Jenis Layanan</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
        <td><strong><?=$res[COL_LAYANANNAMA]?></strong></td>
      </tr>
      <!--<tr>
        <td style="vertical-align: top; width: 200px; white-space: nowrap">NIK</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
        <td><strong><?=$res[COL_TIKETNIK]?></strong></td>
      </tr>-->
      <tr>
        <td style="vertical-align: top; width: 100px; white-space: nowrap">Kelurahan</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
        <td><strong><?=strtoupper($res[COL_REGIONNAMA])?></strong></td>
      </tr>
      <!--<tr>
        <td style="vertical-align: top; width: 200px; white-space: nowrap">Alamat</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
        <td><strong><?=$res[COL_TIKETALAMAT]?></strong></td>
      </tr>-->
      <?php
      if(!empty($rstatus)&&!empty($rstatus[COL_STATUSKETERANGAN])) {
        ?>
        <tr>
          <td style="vertical-align: top; width: 100px; white-space: nowrap">Keterangan</td>
          <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
          <td><strong><?=$rstatus[COL_STATUSKETERANGAN]?></strong></td>
        </tr>
        <?php
      }
      ?>
    </table>
  </div>
  <div class="col-sm-6">
    <table class="table table-striped" style="border: 1px solid #dedede">
      <tr>
        <td style="vertical-align: top; width: 200px; white-space: nowrap">Nama</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
        <td><?=$res[COL_TIKETNAMA]?></td>
      </tr>
      <tr>
        <td style="vertical-align: top; width: 200px; white-space: nowrap">No. HP</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
        <td><?=$res[COL_TIKETHP]?></td>
      </tr>
      <tr>
        <td style="vertical-align: top; width: 200px; white-space: nowrap">Email</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
        <td><?=$res[COL_TIKETEMAIL]?></td>
      </tr>
      <tr>
        <td style="vertical-align: top; width: 200px; white-space: nowrap">Maksud</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
        <td><?=$res[COL_TIKETMAKSUD]?></td>
      </tr>
      <tr>
        <td style="vertical-align: top; width: 200px; white-space: nowrap">Tujuan</td>
        <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
        <td><?=$res[COL_TIKETTUJUAN]?></td>
      </tr>
    </table>
  </div>
</div>
<script type="text/text/javascript">
$(document).ready(function(){
  $('.btn-rate').click(function(){
    var rate = $(this).data('rating');
    var href = $(this).attr('href');
    swal("Apakah anda yakin ingin memberikan pernyataan "+rate+"?", {
      buttons: ["BATAL", "LANJUTKAN"],
    }).then((ok) => {
      if (ok) {
        location.href=href;
      } else {

      }
    });
    return false;
  });

});
</script>
