<?php
$rregion = $this->db->where(COL_ISDELETED,0)->order_by(COL_REGIONNAMA)->get(TBL_MREGION)->result_array();
$rlayanan = $this->db->where(COL_ISDELETED,0)->order_by(COL_LAYANANNAMA)->get(TBL_MLAYANAN)->result_array();
?>
<div class="container">
  <div class="row">
    <div class="col-sm-12">
      <div class="card">
        <div class="card-header">
          <ul class="nav nav-tabs card-header-tabs">
            <li class="nav-item">
              <a class="nav-link <?=!empty($ticketno)?'':'active'?>" data-toggle="tab" href="#tab-form"><i class="far fa-plus-circle"></i>&nbsp;BUAT TIKET</a>
            </li>
            <li class="nav-item">
              <a class="nav-link <?=!empty($ticketno)?'active':''?>" data-toggle="tab" href="#tab-info"><i class="far fa-search"></i>&nbsp;LIHAT STATUS TIKET</a>
            </li>
          </ul>
        </div>
        <div class="card-body">
          <div class="tab-content">
            <div class="tab-pane <?=!empty($ticketno)?'':'active'?>" id="tab-form">
              <form id="form-ticket" action="<?=site_url('site/ticket/add')?>" method="post">
                <div class="row">
                  <div class="col-sm-12">
                    <h5 class="mb-0">Data Diri</h5>
                    <p class="text-muted font-italic">
                      <small>Silakan isi dan lengkapi data sesuai identitas diri anda.</small>
                    </p>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-form-label">Nama Lengkap</label>
                      <input type="text" class="form-control" name="<?=COL_TIKETNAMA?>" placeholder="Nama Lengkap" required />
                    </div>
                  </div>
                  <!--<div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-form-label">NIK</label>
                      <input type="text" class="form-control" name="<?=COL_TIKETNIK?>" placeholder="Nomor Induk Kependudukan" required />
                    </div>
                  </div>-->
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-form-label">Kelurahan</label>
                      <select class="custom-select" name="<?=COL_IDREGION?>" required>
                        <?php
                        foreach ($rregion as $r) {
                          echo '<option value="'.$r[COL_UNIQ].'">'.$r[COL_REGIONNAMA].'</option>';
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                  <!--<div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-form-label">Alamat</label>
                      <input type="text" class="form-control" name="<?=COL_TIKETALAMAT?>" placeholder="Alamat Tempat Tinggal" required />
                    </div>
                  </div>-->
                  <div class="col-sm-3">
                    <div class="form-group">
                      <label class="col-form-label">Email</label>
                      <input type="email" class="form-control" name="<?=COL_TIKETEMAIL?>" placeholder="Alamat Email" required />
                    </div>
                  </div>
                  <div class="col-sm-3">
                    <div class="form-group">
                      <label class="col-form-label">No. HP / Whatsapp</label>
                      <input type="text" class="form-control" name="<?=COL_TIKETHP?>" placeholder="No. HP / Whatsapp" required />
                    </div>
                  </div>
                </div>
                <div class="row mt-4">
                  <div class="col-sm-12">
                    <h5 class="mb-0">Lampiran</h5>
                    <p class="text-muted font-italic">
                      <small>Silakan unggah file / dokumen dengan format <strong>PDF</strong> / <strong>JPG</strong> / <strong>JPEG</strong> / <strong>PNG</strong> dengan ukuran maksimum <strong>2 MB</strong>.</small>
                    </p>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-form-label">Kartu Tanda Penduduk</label>
                      <div class="custom-file mb-1">
                        <input type="file" class="custom-file-input" name="file-ktp" accept=".jpg,.png,.jpeg,.pdf" required>
                        <label class="custom-file-label" for="customFile">Unggah KTP</label>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-form-label">Kartu Keluarga</label>
                      <div class="custom-file mb-1">
                        <input type="file" class="custom-file-input" name="file-kk" accept=".jpg,.png,.jpeg,.pdf" required>
                        <label class="custom-file-label" for="customFile">Unggah KK</label>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row mt-4">
                  <div class="col-sm-12">
                    <h5 class="mb-0">Layanan</h5>
                    <p class="text-muted font-italic">
                      <small>Silakan isi dan lengkapi data sesuai jenis layanan yang anda inginkan.</small>
                    </p>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-form-label">Layanan</label>
                      <select class="custom-select" name="<?=COL_IDLAYANAN?>" required>
                        <?php
                        foreach ($rlayanan as $r) {
                          echo '<option value="'.$r[COL_UNIQ].'">'.$r[COL_LAYANANNAMA].'</option>';
                        }
                        ?>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-form-label">Maksud</label>
                      <textarea class="form-control" name="<?=COL_TIKETMAKSUD?>" placeholder="Misal: Melengkapi berkas administrasi lamaran kerja" required></textarea>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <label class="col-form-label">Tujuan</label>
                      <textarea class="form-control" name="<?=COL_TIKETTUJUAN?>" placeholder="Misal: PT. Maju Jaya Sentosa" required ></textarea>
                    </div>
                  </div>
                </div>
                <div class="row mt-4">
                  <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-pill btn-info"><i class="far fa-check-circle"></i> SUBMIT TIKET</button>
                  </div>
                </div>
              </form>
            </div>
            <div class="tab-pane <?=!empty($ticketno)?'active':''?>" id="tab-info">
              <div class="row">
                <div class="col-sm-12">
                  <form id="form-info">
                    <div class="form-group">
                      <label class="col-form-label">Nomor Tiket</label>
                      <div class="row">
                        <div class="col-sm-6 mb-2">
                          <input type="text" class="form-control" name="<?=COL_TIKETNO?>" placeholder="Nomor Tiket" value="<?=!empty($ticketno)?$ticketno:''?>" required />
                        </div>
                        <div class="col-sm-6 mb-2">
                          <button type="submit" class="btn btn-info"><i class="far fa-arrow-circle-right"></i> LIHAT TIKET</button>
                        </div>
                      </div>
                    </div>
                  </form>
                  <div id="result">

                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  bsCustomFileInput.init();
  $('#form-ticket').validate({
    submitHandler: function(form) {
      swal({
        title: "SUBMIT TIKET?",
        text: "Tekan tombol OK jika data yang anda input sudah benar.",
        icon: "info",
        buttons: true,
        dangerMode: true,
      }).then((confirm) => {
        if (confirm) {
          var btnSubmit = $('button[type=submit]', form);
          var txtSubmit = btnSubmit.html();
          btnSubmit.html('<i class="far fa-circle-notch fa-spin"></i>');
          btnSubmit.attr('disabled', true);

          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                swal("Error", res.error, "error");
              } else {
                if(res.ajax) {
                  $.ajax(res.ajax)
                  .done(function() {})
                  .fail(function() {})
                  .always(function() {});
                }
                swal("Berhasil", res.success, "success").then((value) => {
                  setTimeout(function(){
                    location.href='<?=site_url('site/ticket/index')?>';
                  }, 1000);
                });
              }
            },
            error: function() {
              swal("Error", "Maaf, telah terjadi kesalahan pada server. Silakan coba beberapa saat lagi atau hubungi administrator.", "error");
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
              btnSubmit.attr('disabled', false);
            }
          });
        } else {

        }
      });

      return false;
    }
  });

  $('#form-info').validate({
    submitHandler: function(form) {
      var btnSubmit = $('button[type=submit]', form);
      var txtSubmit = btnSubmit.html();
      btnSubmit.html('<i class="far fa-circle-notch fa-spin"></i>');
      btnSubmit.attr('disabled', true);

      $('#result').load('<?=site_url('site/ticket/info')?>', {TiketNo: $('[name=TiketNo]', form).val()}, function(){
        btnSubmit.html(txtSubmit);
        btnSubmit.attr('disabled', false);
      });
      return false;
    }
  });

  var ticketInfo = $('[name=TiketNo]', $('#form-info')).val();
  if (ticketInfo) {
    $('#form-info').submit();
  }
});
</script>
