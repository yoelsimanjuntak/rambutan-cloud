<?php
$ruser = GetLoggedUser();
$rstatus = $this->db
->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_UNIQ." = ".TBL_TRTIKETSTATUS.".".COL_IDSTATUS,"left")
->join(TBL_MUNIT,TBL_MUNIT.'.'.COL_UNIQ." = ".TBL_TRTIKETSTATUS.".".COL_IDUNIT,"left")
->where(COL_IDTIKET, $data[COL_UNIQ])
->order_by(TBL_TRTIKETSTATUS.'.'.COL_CREATEDON, 'desc')
->get(TBL_TRTIKETSTATUS)
->result_array();
?>
<form id="form-status" action="<?=current_url()?>" method="post">
  <div class="modal-body">
    <?php
    if(!empty($rstatus)&&$rstatus[0][COL_IDSTATUS]==STATUS_PROSES) {
      if($stat==STATUS_SELESAI) {
        if($ruser[COL_ROLEID]!=ROLEADMIN) {
          ?>
          <input type="hidden" name="<?=COL_IDUNIT?>" value="<?=$ruser[COL_UNITID]?>" />
          <?php
        } else {
          ?>
          <div class="form-group">
            <label>LOKASI PENGAMBILAN</label>
            <select class="form-control" name="<?=COL_IDUNIT?>" style="width: 100%">
              <?=GetCombobox("select * from munit where IsDeleted=0 order by UnitNama",COL_UNIQ,COL_UNITNAMA)?>
            </select>
          </div>
          <?php
        }
      } else {
        ?>
        <div class="form-group">
          <label>UNIT KERJA</label>
          <select class="form-control" name="<?=COL_IDUNIT?>" style="width: 100%">
            <?=GetCombobox("select * from munit where IsDeleted=0 and ".($ruser[COL_ROLEID]!=ROLEADMIN?"IdRegion is null":"1=1")." order by UnitNama",COL_UNIQ,COL_UNITNAMA)?>
          </select>
        </div>
        <?php
      }
      ?>
      <?php
    } else {
      if($ruser[COL_ROLEID]!=ROLEADMIN) {
        ?>
        <input type="hidden" name="<?=COL_IDUNIT?>" value="<?=$ruser[COL_UNITID]?>" />
        <?php
      } else {
        ?>
        <div class="form-group">
          <label>UNIT KERJA</label>
          <select class="form-control" name="<?=COL_IDUNIT?>" style="width: 100%">
            <?=GetCombobox("select * from munit where IsDeleted=0 order by UnitNama",COL_UNIQ,COL_UNITNAMA)?>
          </select>
        </div>
        <?php
      }
    }
    ?>
    <div class="form-group">
      <label>KETERANGAN</label>
      <input type="text" class="form-control" name="<?=COL_STATUSKETERANGAN?>" />
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
    <button type="submit" class="btn btn-sm btn-outline-info"><i class="far fa-check-circle"></i>&nbsp;LANJUT</button>
  </div>
</form>
