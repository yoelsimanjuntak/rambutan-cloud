<?php
$ruser = GetLoggedUser();
$rstatus = $this->db
->join(TBL_MSTATUS,TBL_MSTATUS.'.'.COL_UNIQ." = ".TBL_TRTIKETSTATUS.".".COL_IDSTATUS,"left")
->join(TBL_MUNIT,TBL_MUNIT.'.'.COL_UNIQ." = ".TBL_TRTIKETSTATUS.".".COL_IDUNIT,"left")
->where(COL_IDTIKET, $data[COL_UNIQ])
->order_by(TBL_TRTIKETSTATUS.'.'.COL_CREATEDON, 'desc')
->get(TBL_TRTIKETSTATUS)
->result_array();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <?php
          if(!empty($rstatus)) {
            ?>
            <li class="breadcrumb-item"><a href="<?=site_url('site/admin/tiket/'.$rstatus[0][COL_IDSTATUS])?>">Tiket</a></li>
            <?php
          }
          ?>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-body p-0">
            <table class="table table-striped">
              <?php
              if(!empty($rstatus)) {
                $txtStatus = '-';
                if($rstatus[0][COL_IDSTATUS]==STATUS_BARU) $txtStatus = '<span class="badge badge-secondary">'.$rstatus[0][COL_STATUSNAMA].'</span>';
                else if($rstatus[0][COL_IDSTATUS]==STATUS_PROSES) $txtStatus = '<span class="badge badge-primary">'.$rstatus[0][COL_STATUSNAMA].'</span>';
                else if($rstatus[0][COL_IDSTATUS]==STATUS_SELESAI) $txtStatus = '<span class="badge badge-success">'.$rstatus[0][COL_STATUSNAMA].'</span>';
                else if($rstatus[0][COL_IDSTATUS]==STATUS_DISERAHKAN) $txtStatus = '<span class="badge badge-info">'.$rstatus[0][COL_STATUSNAMA].'</span>';
                else if($rstatus[0][COL_IDSTATUS]==STATUS_DITOLAK) $txtStatus = '<span class="badge badge-danger">'.$rstatus[0][COL_STATUSNAMA].'</span>';
                ?>
                <?php
                if($rstatus[0][COL_IDSTATUS]!=STATUS_DITOLAK && $rstatus[0][COL_IDSTATUS]!=STATUS_DISERAHKAN) {
                  ?>
                  <tr>
                    <td style="vertical-align: top; width: 200px; white-space: nowrap">UBAH STATUS</td>
                    <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                    <td>
                      <?php
                      if($rstatus[0][COL_IDSTATUS]==STATUS_BARU) {
                        ?>
                        <button type="button" class="btn btn-change-stat btn-primary btn-sm" data-stat="<?=STATUS_PROSES?>" data-href="<?=site_url('site/admin/tiket-change-stat/'.$data[COL_UNIQ].'/'.STATUS_PROSES)?>"><i class="far fa-sync"></i>&nbsp;PROSES</button>
                        <button type="button" class="btn btn-change-stat btn-danger btn-sm" data-stat="<?=STATUS_DITOLAK?>" data-href="<?=site_url('site/admin/tiket-change-stat/'.$data[COL_UNIQ].'/'.STATUS_DITOLAK)?>"><i class="far fa-times-circle"></i>&nbsp;TOLAK</button>
                        <?php
                      }
                      ?>
                      <?php
                      if($rstatus[0][COL_IDSTATUS]==STATUS_PROSES) {
                        ?>
                        <button type="button" class="btn btn-change-stat btn-primary btn-sm" data-stat="<?=STATUS_PROSES?>" data-href="<?=site_url('site/admin/tiket-change-stat/'.$data[COL_UNIQ].'/'.STATUS_PROSES)?>"><i class="far fa-sync"></i>&nbsp;DISPOSISI</button>
                        <button type="button" class="btn btn-change-stat btn-success btn-sm" data-stat="<?=STATUS_SELESAI?>" data-href="<?=site_url('site/admin/tiket-change-stat/'.$data[COL_UNIQ].'/'.STATUS_SELESAI)?>"><i class="far fa-check-circle"></i>&nbsp;SELESAI</button>
                        <?php
                      }
                      ?>
                      <?php
                      if($rstatus[0][COL_IDSTATUS]==STATUS_SELESAI) {
                        ?>
                        <button type="button" class="btn btn-change-stat btn-info btn-sm" data-stat="<?=STATUS_DISERAHKAN?>" data-href="<?=site_url('site/admin/tiket-change-stat/'.$data[COL_UNIQ].'/'.STATUS_DISERAHKAN)?>"><i class="far fa-user-check"></i>&nbsp;SERAHKAN</button>
                        <?php
                      }
                      ?>
                    </td>
                  </tr>
                  <?php
                }
                ?>
                <tr>
                  <td style="vertical-align: top; width: 200px; white-space: nowrap">Status</td>
                  <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                  <td><?=$txtStatus?></td>
                </tr>
                <?php
                if(!empty($data[COL_TIKETKEPUASAN])) {
                  ?>
                  <tr>
                    <td style="vertical-align: top; width: 200px; white-space: nowrap">Tanggapan</td>
                    <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                    <td><strong class="<?=$data[COL_TIKETKEPUASAN]=='PUAS'?'text-primary':'text-danger'?>"><?=$data[COL_TIKETKEPUASAN]?></strong></td>
                  </tr>
                  <?php
                }
                ?>
                <?php
                if(!empty($rstatus)&&!empty($rstatus[0][COL_STATUSKETERANGAN])) {
                  ?>
                  <tr>
                    <td style="vertical-align: top; width: 200px; white-space: nowrap">Keterangan</td>
                    <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                    <td><strong><?=$rstatus[0][COL_STATUSKETERANGAN]?></strong></td>
                  </tr>
                  <?php
                }
                ?>
                <?php
                if(!empty($rstatus[0][COL_UNITNAMA])) {
                  ?>
                  <tr>
                    <td style="vertical-align: top; width: 200px; white-space: nowrap">Lokasi</td>
                    <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                    <td><strong><?=$rstatus[0][COL_UNITNAMA]?></strong></td>
                  </tr>
                  <?php
                }
              }
              ?>
              <tr>
                <td style="vertical-align: top; width: 200px; white-space: nowrap">No. Tiket</td>
                <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                <td><strong><?=$data[COL_TIKETNO]?></strong></td>
              </tr>
              <tr>
                <td style="vertical-align: top; width: 200px; white-space: nowrap">Nama</td>
                <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                <td><?=$data[COL_TIKETNAMA]?></strong></td>
              </tr>
              <tr>
                <td style="vertical-align: top; width: 200px; white-space: nowrap">Kelurahan</td>
                <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                <td><strong><?=strtoupper($data[COL_REGIONNAMA])?></strong></td>
              </tr>
              <tr>
                <td style="vertical-align: top; width: 200px; white-space: nowrap">No. HP</td>
                <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                <td><?=$data[COL_TIKETHP]?></td>
              </tr>
              <tr>
                <td style="vertical-align: top; width: 200px; white-space: nowrap">Email</td>
                <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                <td><?=$data[COL_TIKETEMAIL]?></td>
              </tr>
              <tr>
                <td style="vertical-align: top; width: 200px; white-space: nowrap">Jenis Layanan</td>
                <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                <td><strong><?=$data[COL_LAYANANNAMA]?></strong></td>
              </tr>
              <tr>
                <td style="vertical-align: top; width: 200px; white-space: nowrap">Maksud</td>
                <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                <td><?=$data[COL_TIKETMAKSUD]?></td>
              </tr>
              <tr>
                <td style="vertical-align: top; width: 200px; white-space: nowrap">Tujuan</td>
                <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                <td><?=$data[COL_TIKETTUJUAN]?></td>
              </tr>
              <tr>
                <td style="vertical-align: top; width: 200px; white-space: nowrap">Lampiran KTP</td>
                <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                <td>
                  <?php
                  if(!empty($data[COL_TIKETFILE1])&&file_exists(MY_UPLOADPATH.$data[COL_TIKETFILE1])) {
                    ?>
                    <a class="btn btn-info btn-xs" href="<?=MY_UPLOADURL.$data[COL_TIKETFILE1]?>" target="_blank"><i class="far fa-download"></i>&nbsp;DOWNLOAD</a>
                    <?php
                  } else {
                    echo '-';
                  }
                  ?>
                </td>
              </tr>
              <tr>
                <td style="vertical-align: top; width: 200px; white-space: nowrap">Lampiran KK</td>
                <td style="vertical-align: top; width: 10px; white-space: nowrap">:</td>
                <td>
                  <?php
                  if(!empty($data[COL_TIKETFILE2])&&file_exists(MY_UPLOADPATH.$data[COL_TIKETFILE2])) {
                    ?>
                    <a class="btn btn-info btn-xs" href="<?=MY_UPLOADURL.$data[COL_TIKETFILE2]?>" target="_blank"><i class="far fa-download"></i>&nbsp;DOWNLOAD</a>
                    <?php
                  } else {
                    echo '-';
                  }
                  ?>
                </td>
              </tr>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-status" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">UBAH STATUS</span>
      </div>
      <div class="modal-ajax-resp">

      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function(){
  $('#modal-status').on('hidden.bs.modal', function (e) {
    $('input, textarea', $('#modal-status')).val('').trigger('change');
    $('.form-group', $('#modal-status')).show().attr('required', true);
    $('form', $('#modal-status')).attr('action', '');
  });

  $('.btn-change-stat').click(function(){
    var stat = $(this).data('stat');
    var href = $(this).data('href');
    if(!stat) {
      return false;
    }

    $('.modal-ajax-resp', $('#modal-status')).load(href, function(){
      $("select", $('#modal-status')).not('.no-select2, .custom-select').select2({ width: 'resolve', theme: 'bootstrap4' });
      $('#modal-status').modal('show');
      $('form', $('#modal-status')).validate({
        submitHandler: function(form) {
          var btnSubmit = $('button[type=submit]', form);
          var txtSubmit = btnSubmit.html();
          btnSubmit.html('<i class="far fa-circle-notch fa-spin"></i>');
          btnSubmit.attr('disabled', true);

          $(form).ajaxSubmit({
            dataType: 'json',
            type : 'post',
            success: function(res) {
              if(res.error != 0) {
                toastr.error(res.error);
              } else {
                toastr.success(res.success);
                if(res.redirect) {
                  setTimeout(function(){
                    location.href=res.redirect;
                  }, 1000);
                }
              }
            },
            error: function() {
              toastr.error("Maaf, telah terjadi kesalahan pada server. Silakan coba beberapa saat lagi atau hubungi administrator.");
            },
            complete: function() {
              btnSubmit.html(txtSubmit);
              btnSubmit.attr('disabled', false);
            }
          });
          return false;
        }
      });
    });

    /*if(stat=='<?=STATUS_PROSES?>' || stat=='<?=STATUS_SELESAI?>') {
      $('[name=IdUnit]', $('#form-status')).closest('.form-group').show().attr('required', true);
    } else {
      $('[name=IdUnit]', $('#form-status')).closest('.form-group').hide().attr('required', false);
    }

    if(stat=='<?=STATUS_DISERAHKAN?>') {
      $('[name=TiketKepuasan]', $('#form-status')).closest('.form-group').show().attr('required', true);
    } else {
      $('[name=TiketKepuasan]', $('#form-status')).closest('.form-group').hide().attr('required', false);
    }*/
  });

});
</script>
