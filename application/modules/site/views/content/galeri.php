<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
div.filtering .select2-container {
  display: inline-block !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/content/galeri-form/add')?>" type="button" class="btn btn-tool btn-add text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH</a>
              <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
            </div>
          </div>
          <div class="card-body">
            <form id="dataform" method="post" action="#">
              <table id="datalist" class="table table-bordered">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 10px">#</th>
                    <th>Judul</th>
                    <th>Keterangan</th>
                  </tr>
                </thead>
                <tbody></tbody>
              </table>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-doc">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Galeri</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true"><i class="fas fa-close"></i></span>
          </button>
        </div>
        <div class="modal-body">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
        </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var modal = $('#modal-doc');
  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/content/galeri-load')?>",
      "type": 'POST',
      "data": function(data){
        //data.filterKategori = $('[name=filterKategori]', $('.filtering')).val();
        //data.filterStatus = $('[name=filterStatus]', $('.filtering')).val();
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    "oLanguage": {
      "sSearch": "FILTER "
    },
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8 filtering'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "dom":"R<'row'<'col-sm-12 d-flex'f<'filtering'>>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    "order": [[ 1, "asc" ]],
    "columnDefs": [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[2], "className":'nowrap'}
    ],
    "columns": [
      {"orderable": false,"width": "50px"},
      {"orderable": true},
      {"orderable": true,"width": "100px"}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('[data-toggle="tooltip"]', $(row)).tooltip();
      $('.btn-action', $(row)).click(function() {
        var url = $(this).attr('href');
        if(confirm('Apakah anda yakin?')) {
          $.get(url, function(res) {
            if(res.error != 0) {
              toastr.error(res.error);
            } else {
              toastr.success(res.success);
            }
          }, "json").done(function() {
            dt.DataTable().ajax.reload();
          }).fail(function() {
            toastr.error('SERVER ERROR');
          });
        }
        return false;
      });
      $('.btn-edit', row).click(function() {
        var url = $(this).attr('href');
        modal.modal('show');
        $('.modal-body', modal).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
        $('.modal-body', modal).load(url, function(){
          $('button[type=submit]', modal).unbind('click').click(function(){
            $('form', modal).submit();
          });
        });
        return false;
      });
    },
    "initComplete": function(settings, json) {
      $('input[type=search]', $('#datalist_filter')).removeClass('form-control-sm').attr('placeholder', 'Keyword');
    }
  });

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });

  modal.on('hidden.bs.modal', function (e) {
    $('.modal-body', modal).empty();
  });

  $('.btn-add').click(function() {
    var url = $(this).attr('href');
    modal.modal('show');
    $('.modal-body', modal).html('<p class="font-italic mb-0 text-center"><i class="far fa-circle-notch fa-spin"></i>&nbsp;MEMUAT...</p>');
    $('.modal-body', modal).load(url, function(){
      $('button[type=submit]', modal).unbind('click').click(function(){
        $('form', modal).submit();
      });
    });
    return false;
  });
});
</script>
