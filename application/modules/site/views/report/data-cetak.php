<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=".$this->setting_web_name." - Rekapitulasi Tiket.xls");

$dateFrom = $_GET['dateFrom'];
$dateTo = $_GET['dateTo'];
$idUnit = $_GET['idUnit'];

if(!empty($dateFrom)) {
  $this->db->where('CAST('.TBL_TRTIKET.'.'.COL_CREATEDON.' as DATE) >= ', $dateFrom);
}
if(!empty($dateTo)) {
  $this->db->where('CAST('.TBL_TRTIKET.'.'.COL_CREATEDON.' as DATE) <= ', $dateTo);
}
if(!empty($idUnit)) {
  $this->db->where(TBL_MREGION.'.'.COL_UNIQ, $idUnit);
}

$res = $this->db
->select('trtiket.*, mlayanan.LayananNama, mregion.RegionNama')
->join(TBL_MLAYANAN,TBL_MLAYANAN.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDLAYANAN,"left")
->join(TBL_MREGION,TBL_MREGION.'.'.COL_UNIQ." = ".TBL_TRTIKET.".".COL_IDREGION,"left")
->order_by(TBL_TRTIKET.'.'.COL_CREATEDON, 'asc')
->get(TBL_TRTIKET)
->result_array();

$runit = array();
if(!empty($idUnit)) {
  $runit = $this->db
  ->where(COL_UNIQ, $idUnit)
  ->get(TBL_MREGION)
  ->row_array();
}

?>
<style>
.text{
  mso-number-format:"\@";/*force text*/
}
</style>
<table class="table table-bordered" style="font-size: 9pt !important;" border="1">
  <caption style="text-align: center">
      <h5><?="REKAPITULASI TIKET ".(!empty($runit)?'<br />'.strtoupper($runit[COL_REGIONNAMA]):'')."<br /> PERIODE ".date('d-m-Y', strtotime($_GET['dateFrom'])).' s.d '.date('d-m-Y', strtotime($_GET['dateTo']))?>
  </caption>
  <thead>
    <tr>
      <td>TANGGAL / WAKTU</td>
      <td>NO. TIKET</td>
      <td>LAYANAN</td>
      <td>NAMA</td>
      <th>LAYANAN</th>
      <th>MAKSUD</th>
      <th>TUJUAN</th>
      <th>KELURAHAN</th>
      <th>KEPUASAN</th>
    </tr>
  </thead>
  <tbody>
    <?php
    foreach($res as $r) {
      ?>
      <tr>
        <td style="vertical-align: top"><?=date("Y-m-d H:i", strtotime($r[COL_CREATEDON]))?></td>
        <td style="vertical-align: top" class="text"><?=$r[COL_TIKETNO]?></td>
        <td style="vertical-align: top" class="text"><?=$r[COL_TIKETNAMA]?></td>
        <td style="vertical-align: top" class="text"><?=$r[COL_LAYANANNAMA]?></td>
        <td class="text"><?=$r[COL_TIKETMAKSUD]?></td>
        <td class="text"><?=$r[COL_TIKETTUJUAN]?></td>
        <td style="vertical-align: top" class="text"><?=$r[COL_REGIONNAMA]?></td>
        <td style="vertical-align: top" class="text"><?=$r[COL_TIKETKEPUASAN]?></td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>
