<?php
$ruser = GetLoggedUser();
function getRandomColor() {
  $letters = explode(",", '0,1,2,3,4,5,6,7,8,9,A,B,C,D,E,F');
  $color = '#';
  for($i=0;$i< 6;$i++ ) {
    $color.=$letters[rand(0,15)];
  }
  return $color;
}
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header">
              <?=form_open(current_url(),array('role'=>'form','id'=>'main-form','class'=>'form-horizontal','method'=>'get'))?>
              <div class="row">
                <div class="col-sm-12">
                  <div class="form-group row">
                    <label class="control-label col-sm-2 text-left mb-0">LOKASI</label>
                    <div class="col-sm-5">
                      <select class="form-control" name="idUnit" style="width: 100%">
                        <?=GetCombobox("SELECT * FROM mregion ORDER BY RegionNama", COL_UNIQ, COL_REGIONNAMA, null, true, false, '-- SEMUA --')?>
                      </select>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label class="control-label col-sm-2 text-left mb-0">PERIODE</label>
                    <div class="col-sm-2">
                      <input type="text" class="form-control datepicker text-right" name="filterDateFrom" value="<?=date('Y-m-1')?>" />
                    </div>
                    <label class="control-label col-sm-1 mb-0 text-center">s.d</label>
                    <div class="col-sm-2">
                      <input type="text" class="form-control datepicker text-right" name="filterDateTo" value="<?=date('Y-m-d')?>" />
                    </div>
                  </div>
                  <div class="form-group row" style="margin: 0 -20px !important; border-top: 1px solid #dedede">
                    <div class="col-sm-12 pl-3 mt-3">
                      <button type="submit" class="btn btn-outline-primary" title="Lihat"><i class="fa fa-arrow-circle-right"></i> TAMPILKAN</button>
                      <?php
                      if(!empty($_GET)) {
                        ?>
                        <a id="btn-download" style="display: none" download="SIDENI - Statistik Kunjungan.jpg" class="btn btn-outline-success" href=""><i class="far fa-download"></i> DOWNLOAD</a>
                        <?php
                      }
                      ?>
                    </div>
                  </div>
                </div>
              </div>
              <?=form_close()?>
            </div>
          </div>
      </div>
      <?php
      if(!empty($_GET)) {
        $idOPD = $_GET['idUnit'];
        $condOPD = !empty($idOPD)?"region.Uniq=$idOPD":"1=1";
        $dateFrom = $_GET['filterDateFrom'];
        $dateTo = $_GET['filterDateTo'];

        $qkepuasan=@"
        select
        UPPER(TiketKepuasan) as TiketKepuasan,
        COUNT(*) as COUNT
        from trtiket
        where
          $condOPD
        	and CAST(trtiket.CreatedOn as DATE) >= '$dateFrom'
        	and CAST(trtiket.CreatedOn as DATE) <= '$dateTo'
          and trtiket.TiketKepuasan is not null
        group by UPPER(TiketKepuasan)
        order by TiketKepuasan
        ";
        $rkepuasan=$this->db->query($qkepuasan)->result_array();
        $arrBgColor = array();
        $objPieKepuasan = array();
        foreach($rkepuasan as $r) {
          $objPieKepuasan['labels'][] = $r[COL_TIKETKEPUASAN];
          $objPieKepuasan['datasets'][0]['data'][] = $r['COUNT'];
          $objPieKepuasan['datasets'][0]['backgroundColor'][] = $r[COL_TIKETKEPUASAN]=='PUAS'?'#007bff':'#dc3545';
        }

        $qlayanan=@"
        select
        UPPER(mregion.RegionNama) as RegionNama,
        COUNT(*) as COUNT
        from trtiket
        left join mregion on mregion.Uniq = trtiket.IdRegion
        where
          $condOPD
          and CAST(trtiket.CreatedOn as DATE) >= '$dateFrom'
        	and CAST(trtiket.CreatedOn as DATE) <= '$dateTo'
        group by UPPER(mregion.RegionNama)
        order by UPPER(mregion.RegionNama)
        ";
        $rlayanan=$this->db->query($qlayanan)->result_array();
        $arrBgColor = array();
        $objPieLayanan = array();
        foreach($rlayanan as $r) {
          $objPieLayanan['labels'][] = $r[COL_REGIONNAMA];
          $objPieLayanan['datasets'][0]['data'][] = $r['COUNT'];
          $objPieLayanan['datasets'][0]['backgroundColor'][] = getRandomColor();
        }

        $arrBgColor = array();
        $objChartDate = array();
        $objChartDate['datasets'][0]['label'] = 'JLH. TIKET';
        $objChartDate['datasets'][0]['animations'] = array('y'=>array('duration'=>2000, 'delay'=>500));
        $objChartDate['datasets'][0]['backgroundColor'] = '#dc3545';

        $datediff = strtotime($dateTo)-strtotime($dateFrom);
        $dayInterval = round($datediff / (60 * 60 * 24));
        if($dayInterval <= 31) {
          $qdate=@"
          select
          CAST(trtiket.CreatedOn as DATE) as Timestamp,
          COUNT(*) as COUNT
          from trtiket
          where
            $condOPD
          	and CAST(trtiket.CreatedOn as DATE) >= '$dateFrom'
          	and CAST(trtiket.CreatedOn as DATE) <= '$dateTo'
          group by CAST(trtiket.CreatedOn as DATE)
          order by CAST(trtiket.CreatedOn as DATE)
          ";
          $rdate=$this->db->query($qdate)->result_array();

          $arrDate = array();
          $currDate = $dateFrom;
          while($currDate <= $dateTo) {
            $arrDate[] = $currDate;
            $currDate = date('Y-m-d', strtotime($currDate . " +1 days"));
          }

          foreach($arrDate as $d) {
            $objChartDate['labels'][] = date('d-m-y', strtotime($d));
            $searchIdx = array_search($d, array_column($rdate, 'Timestamp'));
            if($searchIdx!==false) {
              $objChartDate['datasets'][0]['data'][] = $rdate[$searchIdx]['COUNT'];
            } else {
              $objChartDate['datasets'][0]['data'][] = 0;
            }
          }
        } else {
          $arrMonth = array(
            1=>'JAN',
            2=>'FEB',
            3=>'MAR',
            4=>'APR',
            5=>'MEI',
            6=>'JUN',
            7=>'JUL',
            8=>'AGT',
            9=>'SEPT',
            10=>'OKT',
            11=>'NOP',
            12=>'DES',
          );
          $arrMonthCount = array();

          $qdate=@"
          select
          MONTH(trtiket.CreatedOn) as Timestamp,
          COUNT(*) as COUNT
          from trtiket
          where
            $condOPD
          	and CAST(trtiket.CreatedOn as DATE) >= '$dateFrom'
          	and CAST(trtiket.CreatedOn as DATE) <= '$dateTo'
          group by MONTH(trtiket.CreatedOn)
          order by MONTH(trtiket.CreatedOn)
          ";
          $rdate=$this->db->query($qdate)->result_array();

          for($i=(int)date('m', strtotime($dateFrom)); $i<=(int)date('m', strtotime($dateTo)); $i++) {
            $objChartDate['labels'][] = $arrMonth[$i];
            $searchIdx = array_search($i, array_column($rdate, 'Timestamp'));
            if($searchIdx!==false) {
              $objChartDate['datasets'][0]['data'][] = $rdate[$searchIdx]['COUNT'];
            } else {
              $objChartDate['datasets'][0]['data'][] = 0;
            }
          }
        }
        ?>
        <div class="col-sm-12">
          <div class="card card-default">
            <div id="report-area">
              <div class="card-header text-center">
                <h5 class="card-title font-weight-bold" style="float: none">STATISTIK TIKET</h5>
                <p class="text-center mb-0">
                  <strong><?=date('d-m-Y', strtotime($dateFrom))?></strong> s.d <strong><?=date('d-m-Y', strtotime($dateTo))?></strong>
                </p>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="card card-info">
                      <div class="card-header">
                        <h3 class="card-title">LOKASI</h3>
                      </div>
                      <div class="card-body">
                        <canvas id="chartLayanan" style="height:30vh"></canvas>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="card card-info">
                      <div class="card-header">
                        <h3 class="card-title">KEPUASAN</h3>
                      </div>
                      <div class="card-body">
                        <canvas id="chartKepuasan" style="height:30vh"></canvas>
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12">
                    <div class="card card-info">
                      <div class="card-header">
                        <h3 class="card-title">JUMLAH TIKET</h3>
                      </div>
                      <div class="card-body">
                        <canvas id="chartDate" style="height:30vh"></canvas>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</section>
<script type="text/javascript">
$(document).ready(function() {
  var pieCanvasLayanan = $('#chartLayanan').get(0).getContext('2d');
  var pieCanvasKepuasan = $('#chartKepuasan').get(0).getContext('2d');
  var chartCanvasDate = $('#chartDate').get(0).getContext('2d');

  var pieLayanan = new Chart(pieCanvasLayanan, {
    type: 'pie',
    data: <?=json_encode($objPieLayanan)?>,
    options: {
      maintainAspectRatio : true,
      responsive : true,
      plugins: {
        legend: {
          position: 'right',
          align: 'start'
        }
      }
    }
  });
  var pieKepuasan = new Chart(pieCanvasKepuasan, {
    type: 'pie',
    data: <?=json_encode($objPieKepuasan)?>,
    options: {
      maintainAspectRatio : true,
      responsive : true,
      plugins: {
        legend: {
          position: 'right',
          align: 'start'
        }
      }
    }
  });

  var chartDate = new Chart(chartCanvasDate, {
    type: 'line',
    data: <?=json_encode($objChartDate)?>,
    options: {
      responsive : true
    }
  });

});
</script>
