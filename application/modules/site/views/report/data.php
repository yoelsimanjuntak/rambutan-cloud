<?php
/**
 * Created by PhpStorm.
 * User: Partopi Tao
 * Date: 30/01/2020
 * Time: 22:01
 */
$ruser = GetLoggedUser();
?>
<style>
div.dataTables_info {
    padding-top: 0 !important;
}
div.dataTables_length label {
    margin-bottom: 0 !important;
}
</style>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>

<!-- Main content -->
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
          <div class="card card-default">
            <div class="card-header">
              <div class="card-tools text-center" style="float: none !important">
                <button type="button" class="btn btn-tool btn-refresh-data"><i class="fas fa-sync-alt"></i>&nbsp;REFRESH</button>
              </div>
            </div>
            <div class="card-body">
              <form id="dataform" method="post" action="#">
                <table id="datalist" class="table table-bordered table-hover table-condensed">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>WAKTU</th>
                      <th>NO. TIKET</th>
                      <th>NAMA</th>
                      <th>LAYANAN</th>
                      <th>KELURAHAN</th>
                      <th>KEPUASAN</th>
                    </tr>
                  </thead>
                  <tbody></tbody>
                </table>
              </form>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>
<div id="dom-filter" class="d-none">
  <div class="form-group row">
    <label class="col-sm-2 mb-0">LOKASI</label>
    <div class="col-sm-5">
      <select class="form-control no-select2" name="filterIdOPD" style="width: 100%">
        <?=GetCombobox("SELECT * FROM mregion ORDER BY RegionNama", COL_UNIQ, COL_REGIONNAMA, null, true, false, '-- SEMUA --')?>
      </select>
    </div>
  </div>
  <div class="form-group row">
    <label class="col-sm-2 mb-0">PERIODE</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateFrom" value="<?=date('Y-m-01')?>" />
    </div>
    <label class="col-sm-1 mb-0 text-center">s.d</label>
    <div class="col-sm-2">
      <input type="text" class="form-control form-control-sm datepicker text-right" name="filterDateTo" value="<?=date('Y-m-d')?>" />
    </div>
  </div>
</div>
<div class="modal fade" id="modal-info" role="dialog">
  <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">RINCIAN</h5>
      </div>
      <div class="modal-body p-0">

      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  var modalData = $('#modal-info');

  var dt = $('#datalist').dataTable({
    "autoWidth" : false,
    "processing": true,
    "serverSide": true,
    "ajax": {
      "url": "<?=site_url('site/report/data-load')?>",
      "type": 'POST',
      "data": function(data){
          var dateFrom = $('[name=filterDateFrom]', $('.datefilter')).val();
          var dateTo = $('[name=filterDateTo]', $('.datefilter')).val();
          var idOPD = $('[name=filterIdOPD]', $('.datefilter')).val();

          data.dateFrom = dateFrom;
          data.dateTo = dateTo;
          data.idUnit = idOPD;
       }
    },
    "scrollY" : '40vh',
    "scrollX": "200%",
    "iDisplayLength": 100,
    //"aLengthMenu": [[100, 1000, 5000, -1], [100, 1000, 5000, "Semua"]],
    //"dom":"R<'row'<'col-sm-8'l><'col-sm-4'f>><'row'<'col-sm-12'tr>><'row'<'col-sm-5'i><'col-sm-7'p>>",
    "dom":"R<'row'<'col-sm-8 datefilter'><'col-sm-4 text-right'f<'clear'>B>><'row'<'col-sm-12'tr>><'row'<'col-sm-4'l><'col-sm-4'i><'col-sm-4'p>><'clear'>",
    //"buttons": ['copyHtml5','excelHtml5','csvHtml5','pdfHtml5'],
    buttons: [
      {
        text: '<i class="far fa-download"></i>&nbsp;UNDUH',
        action: function ( e, dt, node, config ) {
          var dateFrom = $('[name=filterDateFrom]', $('.datefilter')).val();
          var dateTo = $('[name=filterDateTo]', $('.datefilter')).val();
          var idOPD = $('[name=filterIdOPD]', $('.datefilter')).val();

          window.open('<?=site_url('site/report/data-cetak')?>'+'?dateFrom='+dateFrom+'&dateTo='+dateTo+'&idUnit='+idOPD, '_blank');
        }
      }
    ],
    "order": [[ 1, "desc" ]],
    "columnDefs": [
      {"targets":[0], "className":'nowrap text-center'},
      {"targets":[1], "className":'nowrap dt-body-right'},
      {"targets":[2,3,4,5], "className":'nowrap'}
    ],
    "columns": [
      {"orderable": false,"width": "10px"},
      {"orderable": true,"width": "50px"},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": true},
      {"orderable": false}
    ],
    "createdRow": function(row, data, dataIndex) {
      $('.btn-view', $(row)).click(function(){
        var href = $(this).attr('href');

        $('.modal-body', modalData).html('<p class="p-2 text-center">MEMUAT...</p>');
        $('.modal-body', modalData).load(href, function(){
          modalData.modal('show');
        });
        return false;
      });
    }
  });

  $("div.datefilter").html($('#dom-filter').html());
  $("select", $("div.datefilter")).select2({ width: 'resolve', theme: 'bootstrap4' });

  $('input,select', $("div.datefilter")).change(function() {
    dt.DataTable().ajax.reload();
  });

  $('.btn-refresh-data').click(function() {
    dt.DataTable().ajax.reload();
  });
});
</script>
