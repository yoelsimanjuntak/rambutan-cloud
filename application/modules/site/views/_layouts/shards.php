<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?=!empty($title)?$title.' - '.$this->setting_web_name:$this->setting_web_name?></title>
    <link rel="icon" type="image/png" href="<?=base_url().$this->setting_web_icon?>">
    <meta name="description" content="<?=$this->setting_web_name?>">
		<meta name="author" content="Partopi Tao">
		<meta name="keyword" content="kecamatan, rambutan, tebing, tebing tinggi, website, pemerintah">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta property="og:title" content="<?=$this->setting_web_name?>" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="<?=base_url()?>" />
		<meta property="og:image" content="<?=base_url().$this->setting_web_logo?>" />

    <!-- CSS Dependencies -->
    <link href="<?=base_url()?>assets/themes/shards/dist/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/shards/dist/css/shards.css?v=3.0.0">
    <link rel="stylesheet" href="<?=base_url()?>assets/themes/shards/dist/css/shards-extras.min.css?version=3.0.0">

    <script src="<?=base_url()?>assets/themes/shards/dist/js/jquery-3.2.1.min.js"></script>
    <style>
    html {
      scroll-behavior: smooth;
    }
    .section-title:after {
      background: #00b8d8 !important;
    }
    label.error {
      font-size: 10pt;
      color: #c4183c;
    }
    .se-pre-con {
        position: fixed;
        left: 0px;
        top: 0px;
        width: 100%;
        height: 100%;
        z-index: 9999;
        background: url(<?=base_url().$this->setting_web_preloader?>) center no-repeat #fff;
    }
    </style>
    <script>
    $(window).on('load', function(){
      $(".se-pre-con").fadeOut("slow");
    });
    </script>
</head>
<body>
  <div class="se-pre-con"></div>
  <div class="wrapper">
    <nav class="navbar navbar-main navbar-expand-lg navbar-dark bg-info">
      <div class="container">
        <a class="navbar-brand" href="<?=base_url()?>">
          <img src="<?=base_url().$this->setting_web_icon?>" class="float-left d-block" style="margin-top: .05rem !important" alt="Pemerintah Kota Tebing Tinggi">
          <span style="line-height: 1; display: block; margin-left: 2.5rem; padding-top: .1rem !important"><span class="font-weight-bold"><?=$this->setting_web_name?></span><br /><small class="font-weight-light"><?=$this->setting_web_desc?></small></span>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown-1" aria-controls="navbarNavDropdown-1" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown-1">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link" href="<?=site_url()?>">Beranda
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?=site_url('site/user/login')?>">Login
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="content-wrapper" style="background: #f9fafc">
      <div class="content py-5">
        <?=$content?>
      </div>
    </div>
    <footer>
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
          <a class="navbar-brand" href="<?=base_url()?>">&copy; 2022</a>
          <span class="right text-white">Partopi Tao</span>
        </div>
      </nav>
    </footer>
  </div>
  <script async defer src="<?=base_url()?>assets/themes/shards/dist/js/buttons.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/function.js"></script>
  <script type="text/javascript" src="<?=base_url()?>assets/template/js/jquery.form.js"></script>
  <script src="<?=base_url()?>assets/themes/shards/dist/js/popper.min.js"></script>
  <script src="<?=base_url()?>assets/themes/shards/dist/js/bootstrap.min.js"></script>
  <script src="<?=base_url()?>assets/themes/shards/dist/js/shards.min.js"></script>

  <script src="<?=base_url()?>assets/themes/shards/dist/js/sweetalert.min.js"></script>
  <script src="<?=base_url()?>assets/themes/adminlte-new/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $('.content-wrapper').css('min-height', $(window).height()-$('.navbar-main').outerHeight()-$('footer').outerHeight());
    $('a.nav-link[href="<?=current_url()?>"]').addClass('active');
  });
  </script>
</body>
</html>
