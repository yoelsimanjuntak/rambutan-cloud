# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: tt_rambutan_20220915
# Generation Time: 2022-09-17 06:13:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table _postcategories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postcategories`;

CREATE TABLE `_postcategories` (
  `PostCategoryID` int(10) unsigned NOT NULL,
  `PostCategoryName` varchar(50) NOT NULL,
  `PostCategoryLabel` varchar(50) DEFAULT NULL,
  `IsShowEditor` tinyint(1) NOT NULL DEFAULT '1',
  `IsAllowExternalURL` tinyint(1) NOT NULL DEFAULT '0',
  `IsDocumentOnly` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postcategories` WRITE;
/*!40000 ALTER TABLE `_postcategories` DISABLE KEYS */;

INSERT INTO `_postcategories` (`PostCategoryID`, `PostCategoryName`, `PostCategoryLabel`, `IsShowEditor`, `IsAllowExternalURL`, `IsDocumentOnly`)
VALUES
	(1,'Berita','#f56954',1,0,0),
	(2,'Infografis','#00a65a',0,0,0),
	(3,'Dokumen','#f39c12',0,0,1),
	(5,'Lainnya','#3c8dbc',1,0,0);

/*!40000 ALTER TABLE `_postcategories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _postimages
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_postimages`;

CREATE TABLE `_postimages` (
  `PostImageID` bigint(20) NOT NULL AUTO_INCREMENT,
  `PostID` bigint(20) NOT NULL,
  `ImgPath` text NOT NULL,
  `ImgDesc` varchar(250) NOT NULL,
  `ImgShortcode` varchar(50) NOT NULL,
  `IsHeader` tinyint(1) NOT NULL DEFAULT '1',
  `IsThumbnail` tinyint(1) NOT NULL DEFAULT '1',
  `Description` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`PostImageID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_postimages` WRITE;
/*!40000 ALTER TABLE `_postimages` DISABLE KEYS */;

INSERT INTO `_postimages` (`PostImageID`, `PostID`, `ImgPath`, `ImgDesc`, `ImgShortcode`, `IsHeader`, `IsThumbnail`, `Description`)
VALUES
	(2,2,'303609040_578211124001195_6619302635285743458_n.jpeg','','',1,1,NULL),
	(3,3,'300269554_568436174978690_1417154352822936994_n.jpeg','','',1,1,NULL),
	(4,3,'299917625_568435284978779_4553657042155356832_n.jpeg','','@GAMBAR1@',0,0,NULL),
	(5,4,'298576672_563806102108364_8961737720311701343_n.jpeg','','',1,1,NULL),
	(6,4,'298705536_563805355441772_5246663762079630750_n.jpeg','','@GAMBAR1@',0,0,NULL);

/*!40000 ALTER TABLE `_postimages` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table _posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `_posts`;

CREATE TABLE `_posts` (
  `PostID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `PostCategoryID` int(11) NOT NULL,
  `PostDate` date NOT NULL,
  `PostTitle` varchar(200) NOT NULL,
  `PostSlug` varchar(200) NOT NULL,
  `PostContent` longtext,
  `PostExpiredDate` date DEFAULT NULL,
  `PostMetaTags` text,
  `IsRunningText` tinyint(1) NOT NULL DEFAULT '0',
  `TotalView` int(11) NOT NULL DEFAULT '0',
  `LastViewDate` datetime DEFAULT NULL,
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '1',
  `FileName` varchar(250) DEFAULT NULL,
  `CreatedBy` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) NOT NULL,
  `UpdatedOn` datetime NOT NULL,
  PRIMARY KEY (`PostID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

LOCK TABLES `_posts` WRITE;
/*!40000 ALTER TABLE `_posts` DISABLE KEYS */;

INSERT INTO `_posts` (`PostID`, `PostCategoryID`, `PostDate`, `PostTitle`, `PostSlug`, `PostContent`, `PostExpiredDate`, `PostMetaTags`, `IsRunningText`, `TotalView`, `LastViewDate`, `IsSuspend`, `FileName`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(2,1,'2022-09-12','Pelaksanaan Kegiatan Kampanye GERMAS di Kota Tebing Tinggi','pelaksanaan-kegiatan-kampanye-germas-di-kota-tebing-tinggi','<p style=\"text-align:justify\">Pelaksanaan Kegiatan Kampanye GERMAS (Gerakan Masyarakat Hidup Sehat) di Kota Tebing tinggi sekaligus penyerahan bantuan beras SIAMOS dari PMI (Palang Merah Indonesia) dalam rangka pencegahan stunting di Kecamatan Rambutan.</p>\r\n\r\n<p style=\"text-align:justify\">Kegiatan dilaksanakan di halaman kantor Kecamatan Rambutan, pada Kamis 1 September 2022. Acara di hadiri oleh Bapak Pj. Walikota Tebing Tinggi, Tim Dari Dinas Kesehatan Provinsi Sumatera Utara, ketua PMI Provinsi Sumatera Utara, Ketua PMI Kota Tebing Tinggi, Kadis Kesehatan Kota Tebing Tinggi, Kepala Bappeda Kota Tebing Tinggi, Danramil-13 Tebing Tinggi, Kapolsek Rambutan, Camat Rambutan, dan Lurah se Kecamatan Rambutan.</p>\r\n',NULL,'germas,tebing,tinggi',0,36,NULL,0,NULL,'admin','2022-09-12 10:42:08','admin','2022-09-12 11:34:11'),
	(3,1,'2022-08-17','Upacara Memperingati HUT Republik Indonesia ke 77','upacara-memperingati-hut-republik-indonesia-ke-77','<p>Upacara mempringati HUT Republik Indonesia ke 77 dilaksanakan di halaman kantor Camat Rambutan Kota Tebing Tinggi, pada hari Selasa, 17 Agustus 2022.</p>\r\n\r\n<p>@GAMBAR1@</p>\r\n\r\n<p>Upacara langsung di pimpin oleh Camat Rambutan, Bapak Marwansyah Harahap, S.STP. Adapun peserta Upacara yaitu :</p>\r\n\r\n<p>- Lurah SeKecamatan Rambutan</p>\r\n\r\n<p>- ASN SeKecamatan Rambutan.</p>\r\n\r\n<p>- Kepala Lingkungan SeKecamatan Rambutan</p>\r\n\r\n<p>- Linmas SeKecamatan Rambutan</p>\r\n\r\n<p>- Tenaga Honorer SeKecamatan Rambutan</p>\r\n\r\n<p>Upacara berlansung dengan lancar dan tertib dengan tetap mematuhi protokol kesehatan.</p>\r\n',NULL,'hut ri,upacara,rambutan',0,0,NULL,0,NULL,'admin','2022-09-12 11:06:31','admin','2022-09-12 11:06:31'),
	(4,1,'2022-09-12','Kegiatan Pencegahan Stunting, Penimbangan dan Pemantauan Tumbuh Kembang Bayi dan Balita','kegiatan-pencegahan-stunting-penimbangan-dan-pemantauan-tumbuh-kembang-bayi-dan-balita','<p>Kegiatan Pencegahan Stunting, Penimbangan dan Pemantauan Tumbuh Kembang Bayi dan Balita usia 0 s.d 60 Bulan di Kelurahan Rantau Laban pada hari Jumat tanggal 12 Agustus 2022 bertempat di Posyandu Kelurahan Rantau Laban. Acara ini turut dihadiri oleh Camat Rambutan Kota Tebing Tinggi. Kegiatan meliputi antara lain :</p>\r\n\r\n<ol>\r\n	<li>Pemberian Vitamin A;</li>\r\n	<li>Imunisasi Lengkap;</li>\r\n	<li>Pemberian PMT dan Penyuluhan;</li>\r\n	<li>Pemberian MPASI kepada Ibu Hamil dan Balita;</li>\r\n	<li>Penimbangan Balita, Tinggi Badan dan Lingkar Kepala;</li>\r\n	<li>Pemberian Obat Cacing; dan</li>\r\n	<li>Penyuluhan.</li>\r\n</ol>\r\n\r\n<p>@GAMBAR1@</p>\r\n\r\n<p>&nbsp;</p>\r\n',NULL,'stunting,balita,bayi,pencegahan',0,7,NULL,0,NULL,'admin','2022-09-12 11:12:11','admin','2022-09-12 11:12:11');

/*!40000 ALTER TABLE `_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mlayanan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mlayanan`;

CREATE TABLE `mlayanan` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `LayananNama` varchar(100) NOT NULL DEFAULT '',
  `LayananDurasi` double DEFAULT NULL,
  `LayananKeterangan` text,
  `CreatedBy` varchar(100) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(100) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mlayanan` WRITE;
/*!40000 ALTER TABLE `mlayanan` DISABLE KEYS */;

INSERT INTO `mlayanan` (`Uniq`, `LayananNama`, `LayananDurasi`, `LayananKeterangan`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(3,'Surat Keterangan',15,'-','admin','2022-09-03 21:06:39','admin','2022-09-03 21:10:56',0);

/*!40000 ALTER TABLE `mlayanan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mregion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mregion`;

CREATE TABLE `mregion` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `RegionNama` varchar(100) NOT NULL DEFAULT '',
  `RegionKeterangan` text,
  `CreatedBy` varchar(100) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(100) NOT NULL DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mregion` WRITE;
/*!40000 ALTER TABLE `mregion` DISABLE KEYS */;

INSERT INTO `mregion` (`Uniq`, `RegionNama`, `RegionKeterangan`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(1,'Tanjung Marulak','-','admin','2022-09-03 12:40:00','admin','2022-09-03 12:42:45',0),
	(2,'Rantau Laban','','admin','2022-09-03 16:45:13','',NULL,0),
	(3,'Sri Padang','','admin','2022-09-03 16:45:27','',NULL,0),
	(4,'Mekar Sentosa','','admin','2022-09-03 16:45:32','',NULL,0),
	(5,'Karya Jaya','','admin','2022-09-03 16:45:37','',NULL,0),
	(6,'Tanjung Marulak Hilir','','admin','2022-09-03 16:45:45','',NULL,0),
	(9,'Lalang','','admin','2022-09-03 16:50:38','',NULL,0);

/*!40000 ALTER TABLE `mregion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mstatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mstatus`;

CREATE TABLE `mstatus` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `StatusNama` varchar(100) DEFAULT NULL,
  `StatusReqUnit` tinyint(1) DEFAULT NULL,
  `StatusReqKeterangan` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mstatus` WRITE;
/*!40000 ALTER TABLE `mstatus` DISABLE KEYS */;

INSERT INTO `mstatus` (`Uniq`, `StatusNama`, `StatusReqUnit`, `StatusReqKeterangan`)
VALUES
	(1,'BARU',NULL,NULL),
	(2,'PROSES',NULL,NULL),
	(3,'SELESAI',NULL,NULL),
	(4,'DISERAHKAN',NULL,NULL),
	(5,'DITOLAK',NULL,NULL);

/*!40000 ALTER TABLE `mstatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table munit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `munit`;

CREATE TABLE `munit` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UnitNama` varchar(100) NOT NULL DEFAULT '',
  `UnitPimpinan` varchar(100) DEFAULT NULL,
  `UnitAlamat` text,
  `IdRegion` bigint(10) unsigned DEFAULT NULL,
  `CreatedBy` varchar(100) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(100) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `munit` WRITE;
/*!40000 ALTER TABLE `munit` DISABLE KEYS */;

INSERT INTO `munit` (`Uniq`, `UnitNama`, `UnitPimpinan`, `UnitAlamat`, `IdRegion`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(1,'Kantor Camat Rambutan','MARWANSYAH HARAHAP, S.STP','',NULL,'admin','2022-09-03 20:38:34',NULL,NULL,0),
	(2,'Kantor Lurah Tanjung Marulak','-','',1,'admin','2022-09-03 20:38:46',NULL,NULL,0),
	(3,'Kantor Lurah Tanjung Marulak Hilir','-','',6,'admin','2022-09-03 20:38:59',NULL,NULL,0),
	(4,'Kantor Lurah Rantau Laban','-','',2,'admin','2022-09-03 20:39:46',NULL,NULL,0),
	(5,'Kantor Lurah Lalang','-','',9,'admin','2022-09-03 20:39:52','admin','2022-09-03 20:40:01',0),
	(6,'Kantor Lurah Sri Padang','-','',3,'admin','2022-09-03 20:40:14',NULL,NULL,0),
	(7,'Kantor Lurah Mekar Sentosa','-','',4,'admin','2022-09-03 20:40:25',NULL,NULL,0),
	(8,'Kantor Lurah Karya Jaya','-','',5,'admin','2022-09-03 20:40:37',NULL,NULL,0);

/*!40000 ALTER TABLE `munit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','SASUKE'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Sistem Aplikasi Surat Keterangan'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','KECAMATAN RAMBUTAN'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. Gn. Leuseur No.2, Tj. Marulak, Kec. Rambutan, Kota Tebing Tinggi, Sumatera Utara 20998'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT','<iframe src=\"https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15931.950628801245!2d99.1651216!3d3.3531616!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5dc46a9831ba3296!2sKantor%20Camat%20Rambutan!5e0!3m2!1sen!2sid!4v1662950343766!5m2!1sen!2sid\" width=\"100%\" height=\"450\" style=\"border:0;\" allowfullscreen=\"\" loading=\"lazy\" referrerpolicy=\"no-referrer-when-downgrade\"></iframe>'),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG','-'),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','0621-21776'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','-'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','kecamatanrambutan@gmail.com'),
	(11,'SETTING_WEB_ICON','SETTING_WEB_ICON','assets/media/image/favicon.png'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','assets/media/image/logo.png'),
	(13,'SETTING_WEB_LOGO2','SETTING_WEB_LOGO2','assets/media/image/logo-full.png'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','assets/preloader/main.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trtiket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trtiket`;

CREATE TABLE `trtiket` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdLayanan` bigint(10) unsigned NOT NULL,
  `IdRegion` bigint(10) unsigned NOT NULL,
  `TiketNo` varchar(100) NOT NULL DEFAULT '',
  `TiketNama` varchar(100) NOT NULL DEFAULT '',
  `TiketNIK` varchar(100) DEFAULT '',
  `TiketAlamat` text,
  `TiketEmail` varchar(100) DEFAULT NULL,
  `TiketHP` varchar(100) DEFAULT NULL,
  `TiketFile1` text,
  `TiketFile2` text,
  `TiketMaksud` text,
  `TiketTujuan` text,
  `TiketKepuasan` varchar(20) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_TIKET_LAYANAN` (`IdLayanan`),
  KEY `FK_TIKET_REGION` (`IdRegion`),
  CONSTRAINT `FK_TIKET_LAYANAN` FOREIGN KEY (`IdLayanan`) REFERENCES `mlayanan` (`Uniq`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TIKET_REGION` FOREIGN KEY (`IdRegion`) REFERENCES `mregion` (`Uniq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `trtiket` WRITE;
/*!40000 ALTER TABLE `trtiket` DISABLE KEYS */;

INSERT INTO `trtiket` (`Uniq`, `IdLayanan`, `IdRegion`, `TiketNo`, `TiketNama`, `TiketNIK`, `TiketAlamat`, `TiketEmail`, `TiketHP`, `TiketFile1`, `TiketFile2`, `TiketMaksud`, `TiketTujuan`, `TiketKepuasan`, `CreatedOn`)
VALUES
	(12,3,1,'202209130001','Yoel Rolas Simanjuntak',NULL,NULL,'yoelrolas@gmail.com','085359867032','CamScanner_09-12-2022_14_47.pdf','CamScanner_09-12-2022_14_471.pdf','Surat Keterangan Domisili','Kampus','PUAS','2022-09-13 23:52:27'),
	(13,3,3,'202209140002','Ranto Sirait',NULL,NULL,'yoelrolas@gmail.com','085359867032','IMAGE_Logos.png','XBANNER_Rambutan_Cloud.png','Testing','Testing',NULL,'2022-09-14 13:54:06'),
	(14,3,1,'202209140003','Yoel Rolas Simanjuntak',NULL,NULL,'yoelrolas@gmail.com','085359867032','QR_SASUKE_1.png','QR_SASUKE_2.png','Testing','Testing',NULL,'2022-09-14 13:56:17'),
	(15,3,3,'202209140004','Ranto Sirait',NULL,NULL,'yoelrolas@gmail.com','085359867032','XBANNER_Rambutan_Cloud1.png','XBANNER_Rambutan_Cloud2.png','Testing','Testing','PUAS','2022-09-14 15:34:43'),
	(16,3,4,'202209140005','RUDI KARO KARO',NULL,NULL,'anggrianijuli25@gmail.com','082361164071','KTP.jpg','KK.jpg','PERMOHONAN BPJS','DINAS SOSIAL','PUAS','2022-09-14 15:53:27'),
	(17,3,5,'202209140006','Husni Hadi Jambak',NULL,NULL,'nilawardani1709@gmail.com','085296032633','20211109_132955.jpg','20211109_1329551.jpg','Melengkapi administrasi mengurus BPJS','Dinas Sosial','PUAS','2022-09-14 15:54:16'),
	(18,3,1,'202209140007','MICHAEL YORDAN CHRISTHEN',NULL,NULL,'iindartysidan@gmail.com','082275051871','WhatsApp_Image_2022-09-14_at_15_49_28.jpeg','WhatsApp_Image_2022-09-14_at_15_49_28(1).jpeg','Melengkapi persyaratan administrasi bantuan beasiswa pendidikan ','Pemko Tebing Tinggi','PUAS','2022-09-14 15:56:14'),
	(19,3,3,'202209140008','Dapot Hamonangan Silalahi',NULL,NULL,'enwa@gmail.com','085830240747','WhatsApp_Image_2022-09-14_at_15_54_31_(1).jpeg','WhatsApp_Image_2022-09-14_at_15_54_31.jpeg','Surat Keterangan Tidak Mampu','ITL Trisakti','PUAS','2022-09-14 16:06:45'),
	(20,3,9,'202209140009','Agus suprianto',NULL,NULL,'subkiiqbal@gmail.com','081211072723','IMG_20220914_154901.jpg','IMG-20220914-WA0026.jpg','Berkas skck','Polsek rambutan','PUAS','2022-09-14 16:24:05'),
	(21,3,6,'202209140010','RISKI',NULL,NULL,'pohanjuliii@gmail.com','085360572153','WhatsApp_Image_2022-09-14_at_16_14_38.jpeg','WhatsApp_Image_2022-09-14_at_16_16_17.jpeg','Melengkapi administrasi pengurusan bpjs','Dinas Sosial Kota Tebing Tinggi ',NULL,'2022-09-14 16:27:54'),
	(22,3,6,'202209140011','RISKI',NULL,NULL,'pohanjuliii@gmail.com','085360572153','WhatsApp_Image_2022-09-14_at_16_14_381.jpeg','WhatsApp_Image_2022-09-14_at_16_16_171.jpeg','Melengkapi administrasi pengurusan bpjs','Dinas Sosial Kota Tebing Tinggi ','PUAS','2022-09-14 16:36:48'),
	(23,3,4,'202209150012','Elia Kurniaty Sianturi',NULL,NULL,'eliakurniatysianturi@gmail.com','082236932016','KTP_Elia_Kurniaty_sianturi.pdf','Kartu_keluarga.pdf','Surat keterangan belum menikah dan belum bekerja','PT Taspen','PUAS','2022-09-15 09:05:59'),
	(24,3,4,'202209150013','Rohaya',NULL,NULL,'kelurahanmekarsentosa22@gmail.com','085762157481','IMG_20220915_120643.jpg','IMG_20220915_120755.jpg','Administrasi umroh ','PT. Gran Darussalam ','PUAS','2022-09-15 12:08:31'),
	(25,3,5,'202209150014','EKO AGUS PRASETYO SE',NULL,NULL,'agusprasetyoeko3@gmail.com','082369296611','IMG-20220914-WA0010.jpeg','16632227979581058094728911288286.jpg','Melengkapi berkas lamaran ke kantor BPS ','Kantor BPS','PUAS','2022-09-15 13:21:36'),
	(26,3,2,'202209150015','Nurmansyah Ali Wardana ',NULL,NULL,'hendri120498@gmail.com','082364201593','WhatsApp_Image_2022-09-15_at_14_12_01.jpeg','WhatsApp_Image_2022-09-15_at_14_12_00.jpeg','Melengkapi Administrasi ','PT Bank Mandiri ',NULL,'2022-09-15 14:19:27'),
	(27,3,5,'202209150016','Rudi Irwasyah',NULL,NULL,'ajjamaharani427@gmail.com','082367443145','16632267049008483052576953404575.jpg','16632267246676004449881862631278.jpg','Izin tidak masuk kerja','PT. Industri Karet Deli',NULL,'2022-09-15 14:27:07'),
	(28,3,6,'202209150017','JALALUDDIN LUBIS',NULL,NULL,'hpnuriati@gmail.com','085262921052','20220915_132611-min.jpg','20220915_132623-min.jpg','Melengkapi administrasi ijin usaha','Pegadaian KUR Syariah',NULL,'2022-09-15 14:43:53'),
	(29,3,3,'202209150018','Erry Lidia Nasution',NULL,NULL,'ewantebing@gmail.com','081265983385','WhatsApp_Image_2022-09-15_at_14_39_25.jpeg','WhatsApp_Image_2022-09-15_at_14_39_19.jpeg','Surat Keterangan Kematian','Bank Mitra Bisnis Keluarga Ventutra',NULL,'2022-09-15 14:47:07');

/*!40000 ALTER TABLE `trtiket` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trtiketstatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trtiketstatus`;

CREATE TABLE `trtiketstatus` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdTiket` bigint(10) unsigned NOT NULL,
  `IdStatus` bigint(10) unsigned NOT NULL,
  `IdUnit` bigint(10) unsigned DEFAULT NULL,
  `StatusKeterangan` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(100) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_STATUS_TIKET` (`IdTiket`),
  CONSTRAINT `FK_STATUS_TIKET` FOREIGN KEY (`IdTiket`) REFERENCES `trtiket` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `trtiketstatus` WRITE;
/*!40000 ALTER TABLE `trtiketstatus` DISABLE KEYS */;

INSERT INTO `trtiketstatus` (`Uniq`, `IdTiket`, `IdStatus`, `IdUnit`, `StatusKeterangan`, `CreatedBy`, `CreatedOn`)
VALUES
	(35,12,1,NULL,NULL,NULL,'2022-09-13 23:52:27'),
	(36,12,2,2,'Tandatangan Lurah','tjmarulak','2022-09-14 00:04:49'),
	(37,12,2,1,'Tandatangan Camat','tjmarulak','2022-09-14 00:06:18'),
	(38,12,3,2,'Sudah bisa diambil','admin','2022-09-14 00:06:57'),
	(39,13,1,NULL,NULL,NULL,'2022-09-14 13:54:06'),
	(40,14,1,NULL,NULL,NULL,'2022-09-14 13:56:17'),
	(41,15,1,NULL,NULL,NULL,'2022-09-14 15:34:43'),
	(42,15,2,6,'Sedang diproses','sripadang','2022-09-14 15:36:53'),
	(43,15,3,6,'Silakan dijemput','sripadang','2022-09-14 15:38:04'),
	(44,15,4,6,'Diserahkan kepada Bpk. XYZ','sripadang','2022-09-14 15:38:42'),
	(45,16,1,NULL,NULL,NULL,'2022-09-14 15:53:27'),
	(46,17,1,NULL,NULL,NULL,'2022-09-14 15:54:16'),
	(47,18,1,NULL,NULL,NULL,'2022-09-14 15:56:14'),
	(48,17,2,8,'Tandatangan Lurah','karyajaya','2022-09-14 16:05:36'),
	(49,19,1,NULL,NULL,NULL,'2022-09-14 16:06:45'),
	(50,17,2,8,'tanda tangan lurah','karyajaya','2022-09-14 16:06:45'),
	(51,16,2,7,'sedang di proses','mekarsentosa','2022-09-14 16:07:05'),
	(52,18,2,2,'tanda tangan lurah','tjmarulak','2022-09-14 16:07:22'),
	(53,17,3,8,'Silakan dijemput','karyajaya','2022-09-14 16:08:36'),
	(54,16,3,7,'silahkan di jemput','mekarsentosa','2022-09-14 16:09:25'),
	(55,19,2,6,'proses','sripadang','2022-09-14 16:11:04'),
	(56,17,4,8,'','karyajaya','2022-09-14 16:11:07'),
	(57,18,3,2,'','tjmarulak','2022-09-14 16:14:36'),
	(58,18,4,2,'telah diserahkan','tjmarulak','2022-09-14 16:20:02'),
	(59,19,3,6,'Diserahkan ke Sdr. Dapot Hamonangan Silalahi','sripadang','2022-09-14 16:20:28'),
	(60,20,1,NULL,NULL,NULL,'2022-09-14 16:24:05'),
	(61,16,4,7,'diterima','mekarsentosa','2022-09-14 16:27:24'),
	(62,21,1,NULL,NULL,NULL,'2022-09-14 16:27:54'),
	(63,17,3,8,'','karyajaya','2022-09-14 16:29:27'),
	(64,20,2,5,'sudah selesai ','lalang','2022-09-14 16:33:19'),
	(65,20,3,5,'silahkan di jemput','lalang','2022-09-14 16:34:44'),
	(66,22,1,NULL,NULL,NULL,'2022-09-14 16:36:48'),
	(67,22,2,3,'sedang di proses','tjmarulakhilir','2022-09-14 16:41:13'),
	(68,22,3,3,'silahkan diambil','tjmarulakhilir','2022-09-14 16:41:32'),
	(69,22,4,3,'serahkan','tjmarulakhilir','2022-09-14 16:42:14'),
	(70,23,1,NULL,NULL,NULL,'2022-09-15 09:05:59'),
	(71,23,2,7,'sedang di proses','mekarsentosa','2022-09-15 09:10:47'),
	(72,23,3,7,'selesai','mekarsentosa','2022-09-15 09:33:14'),
	(73,24,1,NULL,NULL,NULL,'2022-09-15 12:08:31'),
	(74,24,2,7,'sedang di proses','mekarsentosa','2022-09-15 12:08:58'),
	(75,24,3,7,'silahkan di ambil','mekarsentosa','2022-09-15 12:09:16'),
	(76,24,4,7,'diterima','mekarsentosa','2022-09-15 12:11:13'),
	(77,23,4,7,'diterima','mekarsentosa','2022-09-15 12:17:24'),
	(78,25,1,NULL,NULL,NULL,'2022-09-15 13:21:36'),
	(79,25,2,8,'Menunggu Lurah','karyajaya','2022-09-15 13:24:54'),
	(80,25,3,8,'sudah bisa diambil surat domislinya','karyajaya','2022-09-15 13:33:20'),
	(81,25,4,8,'Diserahkan ke Bapak Eko langsung','karyajaya','2022-09-15 13:34:39'),
	(82,26,1,NULL,NULL,NULL,'2022-09-15 14:19:27'),
	(83,26,2,4,'','rantaulaban','2022-09-15 14:22:33'),
	(84,26,3,4,'','rantaulaban','2022-09-15 14:23:22'),
	(85,27,1,NULL,NULL,NULL,'2022-09-15 14:27:07'),
	(86,27,2,8,'Proses menunggu tanda tangan Lurah','karyajaya','2022-09-15 14:31:20'),
	(87,28,1,NULL,NULL,NULL,'2022-09-15 14:43:53'),
	(88,29,1,NULL,NULL,NULL,'2022-09-15 14:47:07');

/*!40000 ALTER TABLE `trtiketstatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleID` tinyint(1) NOT NULL,
  `UnitID` bigint(10) DEFAULT NULL,
  `Username` varchar(50) NOT NULL DEFAULT '',
  `Password` varchar(200) NOT NULL DEFAULT '',
  `Email` varchar(200) DEFAULT '',
  `Fullname` varchar(200) NOT NULL DEFAULT '',
  `Phone` varchar(50) DEFAULT NULL,
  `Gender` enum('MALE','FEMALE','','') DEFAULT NULL,
  `DateBirth` date DEFAULT NULL,
  `IsEmailVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`Uniq`, `RoleID`, `UnitID`, `Username`, `Password`, `Email`, `Fullname`, `Phone`, `Gender`, `DateBirth`, `IsEmailVerified`, `IsSuspend`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,1,NULL,'admin','ec3c0b41532b7899da53d239d3ebd268',NULL,'Administrator',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(2,2,6,'sripadang','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Sri Padang',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(3,2,8,'karyajaya','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Karya Jaya',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(4,2,5,'lalang','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Lalang',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(5,2,7,'mekarsentosa','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Mekar Sentosa',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(6,2,4,'rantaulaban','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Rantau Laban',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(7,2,2,'tjmarulak','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Tanjung Marulak',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(8,2,3,'tjmarulakhilir','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Tanjung Marulak Hilir',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table webcontent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `webcontent`;

CREATE TABLE `webcontent` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ContentType` varchar(50) NOT NULL DEFAULT '',
  `ContentTitle` varchar(200) DEFAULT NULL,
  `ContentDesc1` text,
  `ContentDesc2` text,
  `ContentDesc3` text,
  `ContentDesc4` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `webcontent` WRITE;
/*!40000 ALTER TABLE `webcontent` DISABLE KEYS */;

INSERT INTO `webcontent` (`Uniq`, `ContentType`, `ContentTitle`, `ContentDesc1`, `ContentDesc2`, `ContentDesc3`, `ContentDesc4`)
VALUES
	(25,'Profile','TxtCamat','MARWANSYAH HARAHAP, S.STP','19830330 200212 1 001',NULL,NULL),
	(26,'Profile','TxtPenduduk','38.776','Jiwa',NULL,NULL),
	(27,'Profile','TxtLuas','593,50','Ha',NULL,NULL),
	(28,'Profile','TxtKelurahan','Rantau Laban,Lalang,Sri Padang,Karya Jaya,Mekar Sentosa,Tanjung Marulak,Tanjung Marulak HIlir',NULL,NULL,NULL),
	(30,'Slider','ImgCarousel','slide-1.jpeg,slide-2.jpeg,slide-3.jpeg,slide-4.jpeg',NULL,NULL,NULL);

/*!40000 ALTER TABLE `webcontent` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
