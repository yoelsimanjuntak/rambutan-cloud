# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.38-MariaDB)
# Database: tt_rambutan
# Generation Time: 2022-09-07 03:06:18 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table mlayanan
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mlayanan`;

CREATE TABLE `mlayanan` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `LayananNama` varchar(100) NOT NULL DEFAULT '',
  `LayananDurasi` double DEFAULT NULL,
  `LayananKeterangan` text,
  `CreatedBy` varchar(100) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(100) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mlayanan` WRITE;
/*!40000 ALTER TABLE `mlayanan` DISABLE KEYS */;

INSERT INTO `mlayanan` (`Uniq`, `LayananNama`, `LayananDurasi`, `LayananKeterangan`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(3,'Surat Keterangan',15,'-','admin','2022-09-03 21:06:39','admin','2022-09-03 21:10:56',0);

/*!40000 ALTER TABLE `mlayanan` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mregion
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mregion`;

CREATE TABLE `mregion` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `RegionNama` varchar(100) NOT NULL DEFAULT '',
  `RegionKeterangan` text,
  `CreatedBy` varchar(100) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(100) NOT NULL DEFAULT '',
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mregion` WRITE;
/*!40000 ALTER TABLE `mregion` DISABLE KEYS */;

INSERT INTO `mregion` (`Uniq`, `RegionNama`, `RegionKeterangan`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(1,'Tanjung Marulak','-','admin','2022-09-03 12:40:00','admin','2022-09-03 12:42:45',0),
	(2,'Rantau Laban','','admin','2022-09-03 16:45:13','',NULL,0),
	(3,'Sri Padang','','admin','2022-09-03 16:45:27','',NULL,0),
	(4,'Mekar Sentosa','','admin','2022-09-03 16:45:32','',NULL,0),
	(5,'Karya Jaya','','admin','2022-09-03 16:45:37','',NULL,0),
	(6,'Tanjung Marulak Hilir','','admin','2022-09-03 16:45:45','',NULL,0),
	(9,'Lalang','','admin','2022-09-03 16:50:38','',NULL,0);

/*!40000 ALTER TABLE `mregion` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table mstatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `mstatus`;

CREATE TABLE `mstatus` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `StatusNama` varchar(100) DEFAULT NULL,
  `StatusReqUnit` tinyint(1) DEFAULT NULL,
  `StatusReqKeterangan` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `mstatus` WRITE;
/*!40000 ALTER TABLE `mstatus` DISABLE KEYS */;

INSERT INTO `mstatus` (`Uniq`, `StatusNama`, `StatusReqUnit`, `StatusReqKeterangan`)
VALUES
	(1,'BARU',NULL,NULL),
	(2,'PROSES',NULL,NULL),
	(3,'SELESAI',NULL,NULL),
	(4,'DISERAHKAN',NULL,NULL),
	(5,'DITOLAK',NULL,NULL);

/*!40000 ALTER TABLE `mstatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table munit
# ------------------------------------------------------------

DROP TABLE IF EXISTS `munit`;

CREATE TABLE `munit` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `UnitNama` varchar(100) NOT NULL DEFAULT '',
  `UnitPimpinan` varchar(100) DEFAULT NULL,
  `UnitAlamat` text,
  `CreatedBy` varchar(100) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(100) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  `IsDeleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `munit` WRITE;
/*!40000 ALTER TABLE `munit` DISABLE KEYS */;

INSERT INTO `munit` (`Uniq`, `UnitNama`, `UnitPimpinan`, `UnitAlamat`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`, `IsDeleted`)
VALUES
	(1,'Kantor Camat Rambutan','MARWANSYAH HARAHAP, S.STP','','admin','2022-09-03 20:38:34',NULL,NULL,0),
	(2,'Kantor Lurah Tanjung Marulak','-','','admin','2022-09-03 20:38:46',NULL,NULL,0),
	(3,'Kantor Lurah Tanjung Marulak Hilir','-','','admin','2022-09-03 20:38:59',NULL,NULL,0),
	(4,'Kantor Lurah Rantau Laban','-','','admin','2022-09-03 20:39:46',NULL,NULL,0),
	(5,'Kantor Lurah Lalang','-','','admin','2022-09-03 20:39:52','admin','2022-09-03 20:40:01',0),
	(6,'Kantor Lurah Sri Padang','-','','admin','2022-09-03 20:40:14',NULL,NULL,0),
	(7,'Kantor Lurah Mekar Sentosa','-','','admin','2022-09-03 20:40:25',NULL,NULL,0),
	(8,'Kantor Lurah Karya Jaya','-','','admin','2022-09-03 20:40:37',NULL,NULL,0);

/*!40000 ALTER TABLE `munit` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `settings`;

CREATE TABLE `settings` (
  `SettingID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SettingLabel` varchar(50) NOT NULL,
  `SettingName` varchar(50) NOT NULL,
  `SettingValue` text NOT NULL,
  PRIMARY KEY (`SettingID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;

INSERT INTO `settings` (`SettingID`, `SettingLabel`, `SettingName`, `SettingValue`)
VALUES
	(1,'SETTING_WEB_NAME','SETTING_WEB_NAME','Rambutan Cloud'),
	(2,'SETTING_WEB_DESC','SETTING_WEB_DESC','Aplikasi Layanan Daring Kecamatan Rambutan'),
	(3,'SETTING_WEB_DISQUS_URL','SETTING_WEB_DISQUS_URL','https://general-9.disqus.com/embed.js'),
	(4,'SETTING_ORG_NAME','SETTING_ORG_NAME','KECAMATAN RAMBUTAN'),
	(5,'SETTING_ORG_ADDRESS','SETTING_ORG_ADDRESS','Jl. Merpati Komp. Green House No. 85F Medan'),
	(6,'SETTING_ORG_LAT','SETTING_ORG_LAT','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3982.0046309112786!2d98.6257635145775!3d3.5864108973901536!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x30312f22e90a3dff%3A0xfc1bd7d547d7fee8!2sChayra%20Smart%20Course!5e0!3m2!1sen!2sid!4v1641558439217!5m2!1sen!2sid\" width=\"100%\" height=\"450\" style=\"border:0; box-shadow: 0 2px 48px 0 rgb(0 0 0 / 10%)\" allowfullscreen=\"\" loading=\"lazy\"></iframe>'),
	(7,'SETTING_ORG_LONG','SETTING_ORG_LONG','-'),
	(8,'SETTING_ORG_PHONE','SETTING_ORG_PHONE','628116550507'),
	(9,'SETTING_ORG_FAX','SETTING_ORG_FAX','628116545592'),
	(10,'SETTING_ORG_MAIL','SETTING_ORG_MAIL','c.smartcourse@gmail.com'),
	(11,'SETTING_WEB_ICON','SETTING_WEB_ICON','assets/media/image/favicon.png'),
	(12,'SETTING_WEB_LOGO','SETTING_WEB_LOGO','assets/media/image/logo.png'),
	(13,'SETTING_WEB_LOGO2','SETTING_WEB_LOGO2','assets/media/image/logo-full.png'),
	(14,'SETTING_WEB_PRELOADER','SETTING_WEB_PRELOADER','assets/preloader/main.gif'),
	(15,'SETTING_WEB_VERSION','SETTING_WEB_VERSION','1.0');

/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trtiket
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trtiket`;

CREATE TABLE `trtiket` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdLayanan` bigint(10) unsigned NOT NULL,
  `IdRegion` bigint(10) unsigned NOT NULL,
  `TiketNo` varchar(100) NOT NULL DEFAULT '',
  `TiketNama` varchar(100) NOT NULL DEFAULT '',
  `TiketNIK` varchar(100) NOT NULL DEFAULT '',
  `TiketAlamat` text NOT NULL,
  `TiketEmail` varchar(100) DEFAULT NULL,
  `TiketHP` varchar(100) DEFAULT NULL,
  `TiketFile1` text,
  `TiketFile2` text,
  `TiketMaksud` varchar(200) DEFAULT '',
  `TiketTujuan` varchar(200) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_TIKET_LAYANAN` (`IdLayanan`),
  KEY `FK_TIKET_REGION` (`IdRegion`),
  CONSTRAINT `FK_TIKET_LAYANAN` FOREIGN KEY (`IdLayanan`) REFERENCES `mlayanan` (`Uniq`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `FK_TIKET_REGION` FOREIGN KEY (`IdRegion`) REFERENCES `mregion` (`Uniq`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `trtiket` WRITE;
/*!40000 ALTER TABLE `trtiket` DISABLE KEYS */;

INSERT INTO `trtiket` (`Uniq`, `IdLayanan`, `IdRegion`, `TiketNo`, `TiketNama`, `TiketNIK`, `TiketAlamat`, `TiketEmail`, `TiketHP`, `TiketFile1`, `TiketFile2`, `TiketMaksud`, `TiketTujuan`, `CreatedOn`)
VALUES
	(4,3,5,'202209060001','Yoel Rolas Simanjuntak','1271031208950002','Medan','yoelrolas@gmail.com','085359867032','RESUME_an_Yoel_Rolas_Simanjuntak.pdf','Bukti_pengisian_SP2020_Online.pdf','Ini maksudku apa maksudmu','Ini tujuanku apa tujuanmu','2022-09-06 22:14:23'),
	(5,3,1,'202209070002','Yoel Rolas Simanjuntak','1271031208950002','Medan','yoelrolas@gmail.com','085359867032','RESUME_an_Yoel_Rolas_Simanjuntak1.pdf','Bukti_pengisian_SP2020_Online1.pdf','Lorem Ipsum','Lorem Ipsum','2022-09-07 08:34:00');

/*!40000 ALTER TABLE `trtiket` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table trtiketstatus
# ------------------------------------------------------------

DROP TABLE IF EXISTS `trtiketstatus`;

CREATE TABLE `trtiketstatus` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `IdTiket` bigint(10) unsigned NOT NULL,
  `IdStatus` bigint(10) unsigned NOT NULL,
  `IdUnit` bigint(10) unsigned DEFAULT NULL,
  `StatusKeterangan` varchar(200) DEFAULT NULL,
  `CreatedBy` varchar(100) DEFAULT NULL,
  `CreatedOn` datetime NOT NULL,
  PRIMARY KEY (`Uniq`),
  KEY `FK_STATUS_TIKET` (`IdTiket`),
  CONSTRAINT `FK_STATUS_TIKET` FOREIGN KEY (`IdTiket`) REFERENCES `trtiket` (`Uniq`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `trtiketstatus` WRITE;
/*!40000 ALTER TABLE `trtiketstatus` DISABLE KEYS */;

INSERT INTO `trtiketstatus` (`Uniq`, `IdTiket`, `IdStatus`, `IdUnit`, `StatusKeterangan`, `CreatedBy`, `CreatedOn`)
VALUES
	(4,4,1,NULL,NULL,NULL,'2022-09-06 22:14:23'),
	(7,4,2,1,'Diproses','admin','2022-09-07 08:29:33'),
	(8,4,2,5,'Disposisi','admin','2022-09-07 08:30:31'),
	(9,4,3,1,'Menunggu dijemput','admin','2022-09-07 08:32:23'),
	(10,4,4,NULL,'Sudah diserahkan','admin','2022-09-07 08:32:46'),
	(11,5,1,NULL,NULL,NULL,'2022-09-07 08:34:00'),
	(12,5,5,NULL,'Data tidak lengkap','admin','2022-09-07 08:34:37');

/*!40000 ALTER TABLE `trtiketstatus` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `RoleID` tinyint(1) NOT NULL,
  `UnitID` bigint(10) DEFAULT NULL,
  `Username` varchar(50) NOT NULL DEFAULT '',
  `Password` varchar(200) NOT NULL DEFAULT '',
  `Email` varchar(200) DEFAULT '',
  `Fullname` varchar(200) NOT NULL DEFAULT '',
  `Phone` varchar(50) DEFAULT NULL,
  `Gender` enum('MALE','FEMALE','','') DEFAULT NULL,
  `DateBirth` date DEFAULT NULL,
  `IsEmailVerified` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `IsSuspend` tinyint(1) NOT NULL DEFAULT '0',
  `CreatedBy` varchar(50) NOT NULL DEFAULT '',
  `CreatedOn` datetime NOT NULL,
  `UpdatedBy` varchar(50) DEFAULT NULL,
  `UpdatedOn` datetime DEFAULT NULL,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`Uniq`, `RoleID`, `UnitID`, `Username`, `Password`, `Email`, `Fullname`, `Phone`, `Gender`, `DateBirth`, `IsEmailVerified`, `IsSuspend`, `CreatedBy`, `CreatedOn`, `UpdatedBy`, `UpdatedOn`)
VALUES
	(1,1,NULL,'admin','3798e989b41b858040b8b69aa6f2ce90',NULL,'Administrator',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(2,2,6,'sripadang','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Sri Padang',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(3,2,8,'karyajaya','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Karya Jaya',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(4,2,5,'lalang','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Lalang',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(5,2,7,'mekarsentosa','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Mekar Sentosa',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(6,2,4,'rantaulaban','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Rantau Laban',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(7,2,2,'tjmarulak','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Tanjung Marulak',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL),
	(8,2,3,'tjmarulakhilir','e10adc3949ba59abbe56e057f20f883e',NULL,'Opr. Tanjung Marulak Hilir',NULL,NULL,NULL,0,0,'','0000-00-00 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table webcontent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `webcontent`;

CREATE TABLE `webcontent` (
  `Uniq` bigint(10) unsigned NOT NULL AUTO_INCREMENT,
  `ContentType` varchar(50) NOT NULL DEFAULT '',
  `ContentTitle` varchar(200) DEFAULT NULL,
  `ContentDesc1` text,
  `ContentDesc2` text,
  `ContentDesc3` text,
  `ContentDesc4` text,
  PRIMARY KEY (`Uniq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `webcontent` WRITE;
/*!40000 ALTER TABLE `webcontent` DISABLE KEYS */;

INSERT INTO `webcontent` (`Uniq`, `ContentType`, `ContentTitle`, `ContentDesc1`, `ContentDesc2`, `ContentDesc3`, `ContentDesc4`)
VALUES
	(1,'WelcomeText','WelcomeText','<p style=\"text-align:center\"><span style=\"color:#ffffff\">Platform kursus dengan berbagai fitur dan layanan yang mendukung proses dan persiapan menghadapi ujian / seleksi di tempat impianmu.<br />\r\nAyo bergabung sekarang!</span></p>\r\n\r\n<h2 style=\"text-align:center\"><span style=\"color:#ffffff\"><strong>SIMULASI PSIKOTES CAT</strong></span></h2>\r\n',NULL,NULL,NULL),
	(16,'Galeri','Rifki Aulia Suwandi','','F76A4142-696A-4B20-A2F4-D8189D1784F9.jpeg',NULL,NULL),
	(17,'Galeri','Elia H Duha','','8DAF5315-1648-434B-BEF8-6376C871113E.jpeg',NULL,NULL),
	(18,'Galeri','Adi Sihombing','','926175DD-6228-4F47-93B4-2089B575D753.jpeg',NULL,NULL),
	(19,'Galeri','Daud S. Pane','','B16CC5B3-D494-4F93-AE14-8BE993548316.jpeg',NULL,NULL),
	(20,'Galeri','Eko Sormin','','93109B9F-BB53-4022-8152-CFE88DBF4016.jpeg',NULL,NULL),
	(21,'Galeri','Gracella','','D9B6261D-0B26-4190-8A7C-8B1B895B9EEC.jpeg',NULL,NULL),
	(22,'Galeri','Sbastian Veron Jawak','','97E45BEE-A1C6-4A41-A04F-ECC10C8AFD42.jpeg',NULL,NULL),
	(23,'Galeri','Andre Marpaung','','BE1DF4CB-4797-40DC-98D7-B806FA2E4C48.jpeg',NULL,NULL),
	(24,'Galeri','Jhosua H.S.','','FC83B783-D3A5-4BA5-8DB5-66D99B0C0EDF.jpeg',NULL,NULL);

/*!40000 ALTER TABLE `webcontent` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
